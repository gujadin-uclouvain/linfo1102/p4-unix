#!/usr/bin/env python3
import argparse

parser = argparse.ArgumentParser(description="Afficher le début d'un fichier ou plusieurs fichiers.")
parser.add_argument('-n', '--lines', metavar='N', type=int, default=10, help='Afficher les N premières lignes.')
parser.add_argument('fichier', metavar='fichier', type=str, nargs='+', help='Le fichier à afficher.')

args = parser.parse_args()

for i, fichier in enumerate(args.fichier):
    if len(args.fichier) > 1:
        if i > 0:
            print()
        print('==>', fichier, '<==')
    with open(fichier) as f:
        for l in f.readlines()[:args.lines]:
            print(l[:-1])
