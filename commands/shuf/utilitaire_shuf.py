#Utilitaire shuf
#!/usr/bin/env python3
import argparse
from random import shuffle,choice

analyzer = argparse.ArgumentParser(description = "Melange le contenu d'un fichier")
analyzer.add_argument('-n', "--head-count", metavar='N', type=int, help='Afficher au maximum N lignes.')
analyzer.add_argument("-o", "--output", metavar="FICHIER", type=str, help="Inscrit le melange dans le fichier scpecifier (s'il existe pas, cree un nouveau fichier)")
analyzer.add_argument("-r", "--repeat", action="store_true", help="Ignorer les différences entre majuscules et minuscules lors de la comparaisons des caracteres.")
analyzer.add_argument('fichier', metavar='fichier', type=str, nargs='+', help='Le fichier à traiter.')

args = analyzer.parse_args()
def setup_writing(file):
    """
        pre: file est un string (futur fichier créer par -o)
        post: initialise le fichier (créer s'il existe pas, sinon le remet à zéro pour que le reste du programme fonctionne)
    """
    try:
        with open(file,"w") as f:
            f.write("")
    except:
        with open(file,"x") as f:
            pass

def write_in_file(file,s):
    """
    pre: file est un nom de fichier valide (précédement traité par la fonction setup_writing())
         s est un string (ce qui doit être écrit dans le fichier
    post: Ecrit s à la suite du contenu de celui ci
    """
    with open(file,"r") as f:
        f_start = f.read()
    with open(file,"w") as f:
        f.write(f_start)
        if f_start != "":
            f.write("\n")
        f.write(s)

if args.output is not None: #initialisation du fichier si il y a un "-o"
    setup_writing(args.output)
for i, fichier in enumerate(args.fichier):
    if len(args.fichier) > 1:
        if i > 0:
            print()
        print('==>', fichier, '<==')
    with open(fichier,"r") as file:
        f = (file.read()).split()
        shuffle(f)
        if args.repeat: #si il y a le "-r"
            i = 0
            n = -1 if args.head_count is None else args.head_count #si il y a le "-n" (set la borne maximale)
            while True:
                if n == i: #sortie du "-n"
                    break
                elif args.output is not None: #ecriture si "-o"
                    write_in_file(args.ouput,choice(f))
                else: 
                    print(choice(f))
                i += 1
        else:
            if args.head_count == None: #si pas de "-n"
                n = len(f)
            elif args.head_count > len(f): #si "-n" mais qu'il est plus grand que la taille du fichier
                n = len(f)
            else: # "-n" plus petit que la taille du fichier
                n = args.head_count
            for i in range(n):
                if args.output is not None: #ecriture si "-o"
                    write_in_file(args.output,f[i])
                else:
                    print(f[i])
