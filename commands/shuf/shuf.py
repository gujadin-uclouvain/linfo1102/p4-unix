import subprocess
import unittest
import os

file_text = "a\nb\nc\nd\ne\nf\ng\n"
file_list = ["a\n","b\n","c\n","d\n","e\n","f\n","g\n"]
file_list2 = ["a","b","c","d","e","f","g"]
file_line = 7
def shuf(*args):
    try:
        return subprocess.check_output(['python3', 'projet_final_shuf.py', *args], universal_newlines=True)
    except subprocess.CalledProcessError as e:
        return e.output


class TestShuf(unittest.TestCase):
    def setUp(self):
        with open('test.txt', 'w') as f:
            f.write(file_text)
        with open('test2.txt','w') as f:
            f.write("")

    def tearDown(self):
        os.unlink('test.txt')
        os.unlink('test2.txt')

    def test_shuf(self): #no pipe
        s = shuf('test.txt')
        self.assertNotEqual(s, file_text) #melange ?
        s = s.split()
        self.assertEqual(len(s),file_line) #meme longueur de fichier
        s2 = shuf('test.txt')
        self.assertNotEqual(s2,s) #si 2x shuf
        self.assertEqual(sorted(s),file_list2) #memes éléments

    def test_n(self): # | -n (nombre de lignes)
        for i in [3,5,20]:
            s = shuf('test.txt',"-n",str(i))
            l = s.split()
            len_s = len(s.split())
            self.assertNotEqual(s,file_text.split()[:i]) #si != de head
            if i <= file_line:
                self.assertEqual(len_s,i) #si n < nbre de lignes du fichier
            else:
                self.assertEqual(len_s,file_line) #si n > nbre de ligne du fichier
            for i in l:
                self.assertIn(i,file_list2) #si éléments est dans le fichier

    def test_o(self): # | -o
        s = shuf('test.txt','-o','test2.txt')
        try:
            with open('test2.txt','r') as o:
                file2 = o.read()
                self.assertNotEqual(file_text,file2)
                self.assertEqual(file_line,len(file2.split()))
                f2 = sorted(file2.split())
                self.assertEqual(f2,file_list2)
            s = shuf('test.txt','-o','test.txt')
            with open('test.txt','r') as o:
                file2 = o.read()
                self.assertNotEqual(file_text,file2)
                self.assertEqual(file_line,len(file2.split()))
                f2 = sorted(file2.split())
                self.assertEqual(f2,file_list2)
        except:
            self.fail("Something went wrong with opening file (test_o)") #si erreur avec l'ouverture (fichier inexistant)

    def test_r(self): # | -r
        s = shuf('test.txt','-r','-n','20')
        self.assertNotEqual(s,file_text)
        len_s = len(s.split())
        self.assertEqual(len_s,20)
        l = s.split()
        for i in l:
            self.assertIn(i,file_list2)

if __name__ == '__main__':
    unittest.main()
    pass
