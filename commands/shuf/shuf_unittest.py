import subprocess
import unittest
import os

file_text = "a\nb\nc\nd\ne\nf\ng\n"
file_list2 = ["a","b","c","d","e","f","g"]
file_line = 7
def shuf(*args):
    try:
        return subprocess.check_output(['python3', 'utilitaire_shuf.py', *args], universal_newlines=True)
    except subprocess.CalledProcessError as e:
        return e.output


class TestShuf(unittest.TestCase):
    def setUp(self):
        with open('test.txt', 'w') as f:
            f.write(file_text)
        with open('test2.txt','w') as f:
            f.write("")

    def tearDown(self):
        os.unlink('test.txt')
        os.unlink('test2.txt')

    def test_shuf(self): #no pipe
        s = shuf('test.txt')
        self.assertNotEqual(s, file_text,msg="le programme ne mélange pas") #melange ?
        s = s.split()
        self.assertEqual(len(s),file_line,msg="le programme ne renvoit pas le meme nombre de lignes") #meme longueur de fichier
        s2 = shuf('test.txt')
        self.assertNotEqual(s2,s,"Le programme renvoit 2x la meme chose apres 2 shufs") #si 2x shuf
        self.assertEqual(sorted(s),file_list2,msg = "les élements entre le fichier et la sortie sont pas les memes") #memes éléments

    def test_n(self): # | -n (nombre de lignes)
        for i in [3,20]:
            s = shuf('test.txt',"-n",str(i))
            l = s.split()
            len_s = len(s.split())
            if i <= file_line:
                self.assertEqual(len_s,i,msg = "Renvois pas le bon nombre de lignes") #si n < nbre de lignes du fichier
            else:
                self.assertEqual(len_s,file_line,msg = "Renvois pas le bon nombre de lignes") #si n > nbre de ligne du fichier

    def test_o(self): # | -o
        s = shuf('test.txt','-o','test2.txt')
        try:
            with open('test2.txt','r') as o:
                file2 = o.read()
                self.assertEqual(file_line,len(file2.split()),msg = "fichiers pas de meme taille")
            s = shuf('test.txt','-o','test.txt')
            with open('test.txt','r') as o:
                file2 = o.read()
                self.assertEqual(file_line,len(file2.split()),msg = "fichiers pas de meme taille")
        except:
            self.fail("Something went wrong with opening file (test_o)") #si erreur avec l'ouverture (fichier inexistant)

    def test_r(self): # | -r
        s = shuf('test.txt','-r','-n','5')
        len_s = len(s.split())
        self.assertEqual(len_s,5,msg = "fichier pas de meme taille")

if __name__ == '__main__':
    unittest.main()
    pass
