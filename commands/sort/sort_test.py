import os
import subprocess
import unittest

file1_txt = \
"""2 3 1
1 2 3
3 1 2"""

file1_sort = \
"""1 2 3
2 3 1
3 1 2"""

file1_sort_k2 = \
"""3 1 2
1 2 3
2 3 1"""

file2_txt = \
"""3,2,1
2,1,3
1,3,2"""

file2_sort_t =\
"""2,1,3
3,2,1
1,3,2"""

file3_txt =\
"""2 1 1
1 1 1"""

file3_sort_s =\
"""2 1 1
1 1 1"""

def sort(*args):
    try:
        return subprocess.check_output(['python3', 'sort_uti.py', *args], universal_newlines=True)
    except subprocess.CalledProcessError as e:
        return e.output

class TestSort(unittest.TestCase):
    def setUp(self):

        with open('file1.txt', 'w') as f1:
            with open('file2.txt', 'w') as f2:
                with open('file3.txt', 'w') as f3:
                    f1.write(file1_txt)
                    f2.write(file2_txt)
                    f3.write(file3_txt)

    def tearDown(self):
        os.unlink('file1.txt')
        os.unlink('file2.txt')
        os.unlink('file3.txt')

    def test_sort(self):
        self.assertEqual(sort('file1.txt').strip(),file1_sort.strip())

    def  test_k(self):
        self.assertEqual(sort('file1.txt','-k','2').strip(),file1_sort_k2.strip())

    def test_t(self):
        self.assertEqual(sort('file2.txt','-t',',','-k','2').strip(),file2_sort_t.strip())

    def test_s(self):
        self.assertEqual(sort('file3.txt','-k','2','-s').strip(),file3_sort_s.strip())

if __name__ == '__main__':
    unittest.main()
