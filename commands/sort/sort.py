import argparse

parser = argparse.ArgumentParser(description = "Trie les éléments d'un fichier dans l'ordre alphabétique ou croissant")

parser.add_argument("file", metavar = "FILE", type = str, nargs = "+", help = "Le fichier à trier")
parser.add_argument("-k", "--key", metavar = "K", type = int, default = 1, help = "Trie le fichier en fonction de l'indice indiqué par la clé")
parser.add_argument("-s", "--stable", action = "store_true", help = "Réalise un tri stabilisé. Cela désactive le tri de dernier recours.")
parser.add_argument("-t", "--field_separator", metavar = "T",type = str, default = " ", help = "Permet de définir un nouveau caractère de séparation entre les colonnes")

args = parser.parse_args()

a_list = []
for i, file in enumerate(args.file):
    with open(file) as f:
        for line in f.readlines():                                        #Lecture du fichier et récupération des données dans une liste.
            a_list.append(line.rstrip().split(args.field_separator))
    def sort(lst):
        if args.stable == True and args.key != 1:                         #Renvoie une liste triée de manière stabilisée si l'argument 'stable' est actif.
            return sorted(lst,key = lambda x : x[args.key-1:])
        for i in range(len(a_list)):                                       #Remplissage des éléments de la liste pour un meilleur tri
            while len(a_list[i]) < args.key:
                a_list[i].append("\t")
        output = sorted(a_list,key = lambda x : x[args.key-1:] + x[:args.key]) # Tri de la liste(incluent l'option "-k")
        for i in range(len(a_list)):
            while "\t" in a_list[i]:
                a_list[i].remove("\t")
        return output


x = sort(a_list)
for i in x:                                                               #les éléments triés sont affichés dans leur forme de départ.
    print(args.field_separator.join(i))
