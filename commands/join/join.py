import argparse

# [SECTION ARGPARSE -- DÉBUT]
analyzer = argparse.ArgumentParser(description = """Assemble les lignes de deux fichiers ayant un caractère similaire.  
                                                    Il est IMPÉRATIF que 'FICHIER1' et 'FICHIER2' soient triés à l'index où s'effectue la comparaison !!!""")
analyzer.add_argument("-i", "--ignore_case", action="store_true", help="Ignorer les différences entre majuscules et minuscules lors de la comparaisons des caractères")
analyzer.add_argument("-t", "--sep", metavar="T", type=str, default=" ", help="Utilise le caractère entrée comme nouveau séparateur entre les autres caractères")
analyzer.add_argument("-1", "--file1", metavar="1", type=int, default=1, help="Utilise l'index choisi comme l'emplacement du nouveau caractère à comparer avec le fichier2")
analyzer.add_argument("-2", "--file2", metavar="2", type=int, default=1, help="Utilise l'index choisi comme l'emplacement du nouveau caractère à comparer avec le fichier1")
analyzer.add_argument('fichier1', metavar='FICHIER1', type=str, nargs='+', help='Le premier fichier à comparer')
analyzer.add_argument('fichier2', metavar='FICHIER2', type=str, nargs='+', help='Le second fichier à comparer')

args = analyzer.parse_args()
# [SECTION ARGPARSE -- FIN]

# [SECTION FONCTIONS -- DÉBUT]
def join_action(args, line1, line2):
    """
    L'assemblage pur et dur de chaques lignes.
    ------------------------------------------------
    pre: Les deux lignes à assembler: line1, line2.
         l'accès aux options de l'utilitaire: args
    post: Imprime les deux lignes assemblées.
    """
    printer = line1[args.file1-1]
    for sub_index in range(len(line1)): #ajout venant du fichier 1
        if sub_index != args.file1-1: #Pour ne pas ajouter l'élément comparatif
            printer += args.sep + line1[sub_index]
            
    for sub_index in range(len(line2)): #ajout venant du fichier 2
        if sub_index != args.file2-1: #Pour ne pas ajouter l'élément comparatif
            printer += args.sep + line2[sub_index]
    print(printer)


def join_ignore_case(args, line1, line2):
    """
    Vérifie, en ignorant les différences entre majuscules et minuscules, si les deux caractères à comparer sont égaux ou non. 
    ------------------------------------------------------
    pre: Les deux lignes à vérifier: line1, line2.
         l'accès aux options de l'utilitaire: args
    post: Si line1 et line2 sont égaux:
            - rentre dans la fonction join_action
            - renvoie True
          Sinon:
            - renvoie False
    """
    case_solver_1 = ""
    case_solver_2 = ""
    
    for v, element1 in enumerate(line1[args.file1-1]): # prend chaques lettres/chiffres de la ligne à comparer du fichier 1
        case_solver_1 += element1
        
        for w, element2 in enumerate(line2[args.file2-1]): # prend chaques lettres/chiffres de la ligne à comparer du fichier 2
            if v == w: # permet de synchroniser l'avancée d'index dans les deux lignes des deux fichiers
                if type(element1) == str and type(element2) == str: # vérifie si le caractère est un string ou non
                    if element1 == element2.upper(): # vérifie si l'élément 1 est une majuscule
                        case_solver_2 += element2.upper()
                    elif element1 == element2.lower(): # vérifie si l'élément 1 est une minuscule
                        case_solver_2 += element2.lower()
                        
    if case_solver_1 == case_solver_2:
        join_action(args, line1, line2) # appel de la fonction join_action
        return True
    else:
        return False
# [SECTION FONCTIONS -- FIN]

# [SECTION CODE -- DÉBUT]
for i, fichier1 in enumerate(args.fichier1):
    if len(args.fichier1) > 1:
        if i > 0:
            print()
        print('==>', fichier1, '<==')
        
    for j, fichier2 in enumerate(args.fichier2):
        if len(args.fichier2) > 1:
            if i > 0:
                print()
            print('==>', fichier2, '<==')
        
        with open(fichier1) as f1, open(fichier2) as f2:
            l1 = f1.readlines() # fichier 1 en entier => liste 1 (l1)
            l2 = f2.readlines() # fichier 2 en entier => liste 2 (l2)
            
            counter1 = 0 # Dénini le compteur du fichier 1 à 0.
            counter2 = -1 # Défini le compteur du fichier 2 à -1 (car il s'augmente la 1ère fois en rentrant dans la boucle)
            which_counter = True # Défini quel compteur choisir (True = count2, False = count1).
            run = True # Variable qui évite l'IndexError (elle termine le programme plus tôt)
            
            while run:
                if which_counter == True:
                    counter2 += 1
                    
                elif which_counter == False:
                    counter1 += 1
                    
                if counter1 < len(l1) and counter2 < len(l2): # Évite l'IndexError entre les 2 fichiers.
                    line1 = l1[counter1].strip().split(args.sep) # séparer la ligne sélectionnée en fonction du séparateur établi (-t) pour le fichier 1
                    line2 = l2[counter2].strip().split(args.sep) # séparer la ligne sélectionnée en fonction du séparateur établi (-t) pour le fichier 2
                    
                    if args.file1 <= len(line1) and args.file2 <= len(line2): # fix pour les options -1 et -2
                        if line1[args.file1-1] == line2[args.file2-1]: # comparer les deux éléments des deux fichiers
                            join_action(args, line1, line2) # appel de la fonction join_action
                               
                        else:
                            if args.ignore_case: # dans le cas où l'option -i est activée
                                which_counter = join_ignore_case(args, line1, line2) # appel de la fonction join_ignore_case
                            else:
                                which_counter = not which_counter # si les éléments ne correspondes pas, switch de count1 vers count2 ou de count2 vers count1.
                    else:
                        which_counter = not which_counter # si les éléments ne correspondes pas, switch de count1 vers count2 ou de count2 vers count1.
                else:
                    run = False # Arrête le programme
# [SECTION CODE -- FIN]
