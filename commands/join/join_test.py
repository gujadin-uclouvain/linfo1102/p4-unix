import os
import subprocess
import unittest

file1_txt = \
"""1||a 4
2||b 5
3||c 6
"""

file2_txt = \
"""1||a 7
2||b 8
3||c 9
"""

file3_txt = \
"""1||A 7
2||B 8
3||C 9
"""

default12 = \
"""1||a 4 7
2||b 5 8
3||c 6 9
"""

default21 = \
"""1||a 7 4
2||b 8 5
3||c 9 6
"""

sep = \
"""1||a 4||a 7
2||b 5||b 8
3||c 6||c 9
"""

upper = \
"""1||A 7 7
2||B 8 8
3||C 9 9
"""

columnsdiff = \
"""7 1||a 1||A
8 2||b 2||B
9 3||c 3||C
"""

def join(*args):
    try:
        return subprocess.check_output(['python3', 'join.py', *args], universal_newlines=True)
    except subprocess.CalledProcessError as e:
        return e.output
    
class TestJoin(unittest.TestCase):
    def setUp(self):
        with open('file1.txt', 'w') as f1:
            with open('file2.txt', 'w') as f2:
                with open('file3.txt', 'w') as f3:
                    f1.write(file1_txt.strip())
                    f2.write(file2_txt.strip())
                    f3.write(file3_txt.strip())

    def tearDown(self):
        os.unlink('file1.txt')
        os.unlink('file2.txt')
        os.unlink('file3.txt')

    def test_default12(self):
        self.assertEqual(join('file1.txt', 'file2.txt').strip(), default12.strip())
        
    def test_default21(self):
        self.assertEqual(join('file2.txt', 'file1.txt').strip(), default21.strip())
        
    def test_diff_separation(self):
        self.assertEqual(join('file1.txt', 'file2.txt', '-t', '|').strip(), sep.strip())
        
    def test_lower(self):
        self.assertEqual(join('file1.txt', 'file3.txt', '-i').strip(), default12.strip())
        
    def test_upper(self):
        self.assertEqual(join('file3.txt', 'file2.txt', '-i').strip(), upper.strip())
    
    def test_first_columns(self):
        self.assertEqual(join('file1.txt', 'file2.txt', '-1', '1', '-2', '1').strip(), default12.strip())
    
    def test_second_columns(self):
        self.assertEqual(join('file2.txt', 'file3.txt', '-1', '2', '-2', '2').strip(), columnsdiff.strip())

if __name__ == '__main__':
    unittest.main()
