#Juffern Saskia
import argparse

parser = argparse.ArgumentParser(description="Afficher le début d'un fichier ou plusieurs fichiers.")
parser.add_argument('fichier', type=str, nargs=1, help='Le fichier à afficher.')
parser.add_argument('-c', action='store_true', help='Donne les nombres d occurences d un mot')
parser.add_argument('-d', action='store_true', help='Donne les lignes qui apparaissent plusieurs fois')
parser.add_argument('-u', action='store_true', help='Donne les lignes qui apparaissent qu une seule fois')
parser.add_argument('-s', type=int, help='Ne compare pas les n premiers lettres')
parser.add_argument('-w', type=int, help='Compare seulement les n premiers lignes')

args = parser.parse_args()

dico={}
#faire un dictionnaire avec le nombre d'ocurrences de chaque ligne
with open (args.fichier[0],"r") as f:
    for ligne in f: 
        if ligne in dico:
            dico[ligne] += 1
        else:
            dico[ligne] = 1

if args.c:
    #pour -c, on met la ligne et le nobre en string
    string = ""
    for line,number in dico.items():
        string += "{0} {1}".format(number,line)
    print(string)
    
elif args.d:
    #chaque element dont le nombre d'occurence est superieur de 1 va etre mis dans le string
    string=""
    for line,number in dico.items():
        if number > 1:
            string += "{0}".format(line)
    print(string)

elif args.u:
    #chaque element dont le nombre d'occurence est egal a 1 va etre mis dans le string
    string=""
    for line,number in dico.items():
        if number == 1:
            string += "{0}".format(line)
    print(string)

elif args.s:
    num = args.s
    liste = []     #d'abord je fais une liste avec toutes les lignes (les lignes identiques sont une seule fois dedans)
    for line,number in dico.items():
        liste.append(line)
    
    new = []
    new.append(liste[0])  #on ajoute juste la premiere ligne
    for element in liste[1:]:
        boo = True
        for n in new:     #on compare les lettres de l'index num jusqu'au bout avec les lettres de chaque element de la nouvelle liste
            if element[num:] == n[num:]:
                boo=False    #si les lettres sont identiques, le booleen va etre False
        if boo:    #on regarde si le booleen est true ou false, s'il est true on ajoute l'element a la nouvelle liste
            new.append(element)
    
    string = ""    #on met la nouvelle liste en forme de string
    for i in new:
        string+="{0}".format(i)
    print(string)

elif args.w:
    num = args.w
    liste = []    #d'abord je fais une liste avec toutes les lignes (les lignes identiques sont une seule fois dedans)
    for line,number in dico.items():
        liste.append(line)
        
    new =[]
    new.append(liste[0])  #on ajoute juste la premiere ligne
    for element in liste[1:]:
        boo =True
        for n in new:    #on compare les lettres deu debut jusqu'a l'index num avec les lettres de chaque element de la nouvelle liste
            if element[:num] == n[:num]:
                boo = False    #si les lettres sont identiques, le booleen va etre False
        if boo:    #on regarde si le booleen est true ou false, s'il est true on ajoute l'element a la nouvelle liste
            new.append(element)
            
    string = ""    #on met la nouvelle liste en forme de string
    for i in new:
        string+="{0}".format(i)
    print(string)
    

else:
    string = ""   #si on ne met pas d'argument, il donne juste toutes les lignes (les identiques qu'une seule fois
    for line,number in dico.items():
        string += "{0}".format(line)
    print(string)