from random import *

def shuf(doc):
    """
        pre: 1 fichier d'entrée ayant des lignes dans un certain ordre de base
        post: le contenu du fichier d'entrée est remplacé par les mêmes lignes mélangées
    """
    with open(doc, "r") as f:
        memlst1 = f.readlines()
    memlst2 = []
    for line in memlst1:
        memlst2.append(line.strip("\n"))
    result = []
    memdic = {}
    while len(result) != len(memlst2):
        index = randrange(0, len(memlst2))
        if index not in memdic:
            result.append(memlst2[index])
            memdic[index] = "Done"
    with open(doc, "w") as fout:
        order = 0
        for line in result:
            order += 1
            if order != len(result):
                to_write = ("{0}\n").format (line)
                fout.write(to_write)
            else:
                fout.write(line)

def shuf_o(indoc, outdoc):
    """
        pre: 1 fichier d'entrée ayant des lignes dans un certain ordre de base
        post: 1 fichier de sortie avec les mêmes lignes mais dans un ordre différent
    """
    with open(indoc, "r") as f:
        memlst1 = f.readlines()
    memlst2 = []
    for line in memlst1:
        memlst2.append(line.strip("\n"))
    result = []
    memdic = {}
    while len(result) != len(memlst2):
        index = randrange(0, len(memlst2))
        if index not in memdic:
            result.append(memlst2[index])
            memdic[index] = "Done"
    with open(outdoc, "w") as fout:
        order = 0
        for line in result:
            order += 1
            if order != len(result):
                    to_write = ("{0}\n").format (line)
                fout.write(to_write)
            else:
                fout.write(line)

def shuf_n(indoc, outdoc, nbr):
    """
        pre: 1 fichier d'entrée ayant des lignes dans un certain ordre de base
        post: 1 fichier de sortie avec un certain nombre lignes mélangée, nombre égal ou inférieur au nombre de ligne
    """
    with open(indoc, "r") as f:
        memlst1 = f.readlines()
    memlst2 = []
    for line in memlst1:
        memlst2.append(line.strip("\n"))
    result = []
    memdic = {}
    while len(result) != len(memlst2):
        index = randrange(0, len(memlst2))
        if index not in memdic:
            result.append(memlst2[index])
            memdic[index] = "Done"
    with open(outdoc, "w") as fout:
        order = 0
        for line in result:
            order += 1
            if order != nbr:
                to_write = ("{0}\n").format (line)
                fout.write(to_write)
            else:
                fout.write(line)
                break
            
def shuf_r(indoc, outdoc, nbr):
    """
        pre: 1 fichier d'entrée ayant des lignes dans un certain ordre de base
        post: 1 fichier de sortie avec un certain nombre lignes mélangée, une même ligne peut se répéter
    """
    with open(indoc, "r") as f:
        memlst1 = f.readlines()
    memlst2 = []
    for line in memlst1:
        memlst2.append(line.strip("\n"))
    result = []
    while len(result) != nbr:
        index = randrange(0, len(memlst2))
        result.append(memlst2[index])
    with open(outdoc, "w") as fout:
        order = 0
        for line in result:
            order += 1
            if order != nbr:
                to_write = ("{0}\n").format (line)
                fout.write(to_write)
            else:
                fout.write(line)
                break
shuf("test_shuf.txt")
shuf_o("test.txt", "test_shuf_o.txt")
shuf_n("test.txt", "test_shuf_n.txt", 5)
shuf_r("test.txt", "test_shuf_r.txt", 20)