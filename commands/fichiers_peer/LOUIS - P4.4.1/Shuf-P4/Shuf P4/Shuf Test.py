import unittest
import subprocess
import os
import Shuf

valeur = [0,1,2,3,4,5,6,7,8,9]

class TestHead(unittest.TestCase):
    def Start(self):
        with open("test.txt", "w") as f:
            for nbr in valeur:
                f.write(("{0}\n").format (nbr))
        with open("test_shuf.txt", "w") as f_shuf:
            for nbr in valeur:
                f.write(("{0}\n").format (nbr))
        with open("test_shuf_o", "w") as f_shuf_o:
            shuf_o(f, f_shuf_o)
        with open("test_shuf_n", "w") as f_shuf_n:
            shuf_n(f, f_shuf_n, 5)
        with open("test_shuf_r", "w") as f_shuf_r:
            shuf_r(f, f_shuf_r, 20)
            
    def tearDown(self):
        os.unlink("test.txt")
    
    def test_shuf(self):
        shuf(f_shuf)
        self.assertEqual(sorted(f_shuf.readlines())), (sorted(f.readlines))
        
    def test_shuf_o(self):
        shuf_o(f, f_shuf_o)
        self.assertEqual(sorted(f.readlines()), sorted(f_shuf_o.readlines))
        
    def test_shuf_n(self):
        shuf_n(f, f_shut_n, 5)
        self.assertEqual(len(f_shut.readlines()), 5)
        
    def test_shuf_r(self):
        shut_r(f, f_shut_r, 20)
        self.assertEqual(len(f_shut_r.readlines()), 20)
        
if __name__ == '__main__':
    unittest.main()