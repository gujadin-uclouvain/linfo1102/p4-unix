import os
import subprocess
import unittest

test_txt = '1 Patate'
test_txt2 = '1 Cuite'
test_txt3 = '1, Patate'
test_txt4 = '1, Cuite'
txt = "Salut c'est Pascale"
txt2 = "salut c'est Pascale"
txt3 = "Une pomme de terre"
txt4 = "Une pomme au four"

class TestHead(unittest.TestCase):
    def setUp(self):
        with open('test.txt', 'w') as f:
            f.write(test_txt)
            
        with open('test2.txt', 'w') as f2:
            f2.write(test_txt2)
            
        with open('test3.txt', 'w') as f3:
            f3.write(test_txt3)
            
        with open('test4.txt', 'w') as f4:
            f4.write(test_txt4)
        
        with open('test5.txt', 'w') as f5:
            f5.write(txt)
        
        with open('test6.txt', 'w') as f6:
            f6.write(txt2)
        
        with open('test7.txt', 'w') as f7:
            f7.write(txt3)
        
        with open('test8.txt', 'w') as f8:
            f8.write(txt4)
            
            
    def tearDown(self):
        os.unlink('test.txt')

    def test_join(self):
        self.assertEqual(join(f,f2), '1 Patate Cuite')

    def test_join_t(self):
        self.assertEqual(join('-t',f3,f4), '1, Patate Cuite')
    
    def test_join_i(self):
        self.assertEqual(join('-i',f5 ,f6), "Salut c'est Pascale")

    def test_join_12(self):
        self.assertEqual(join('-1','2','-2','2',f7, f8), "Une pomme de terre au four")
        
if __name__ == '__main__':
    unittest.main()
