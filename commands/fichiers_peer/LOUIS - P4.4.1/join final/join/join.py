n = ","
x = 2
y = 2
def join():
    """
        pre: deux fichiers contenant un certain nombre de lignes.
        post: Si le premier champ est le même, les deux fichiers sont joints. Le premier champ est gardé et ensuite
        le reste du premier fichier et la suite du deuxieme fichier.
    """
    #Les deux premiers champs doivent être parfaitement identiques
    file = open('test.txt', 'r')
    file2 = open('test2.txt', 'r')
    data = file.readlines()
    data2 = file2.readlines()
    for i in range(len(data)):
        data[i] = data[i].strip('\n').split()
        data2[i] = data2[i].strip('\n').split()
        if data[i][0] == data2[i][0]:#Vérifie si le premier champ de chaque ligne est le même pour les deux fichiers
            print("{} {} {}".format(data[i][0]," ".join(data[i][1:])," ".join(data2[i][1:])))
    file.close()
    file2.close()
#-------------------------------------------------------------------------------
def join_t(n):
    """
        pre : Deux fichiers contenant un certain nombre de lignes
        post : Imprime les deux fichiers joints séparés par le caractère n. 
    """
    #Il faut que absolument que les lignes qui possèdent le caractère n suivent cet exemple : 1 , Petit et 1 , Suisse et non 1,petit 1,suisse. C'est à dire doivent être séparés par des espaces.
    file = open('test3.txt', 'r')
    file2 = open('test4.txt', 'r')
    data = file.readlines()
    data2 = file2.readlines()
    for i in range(len(data)):
        data[i] = data[i].strip('\n').split()
        data2[i] = data2[i].strip('\n').split()
        if n in data[i] and n in data2[i]:
            if data[i].index(n) == data2[i].index(n):
                if data[i][0] == data2[i][0]:
                    print("{} {} {} {} {}".format(data[i][0],n,"".join(data[i][data[i].index(n)+1:]),n,"".join(data2[i][data2[i].index(n)+1:])))
    file.close()
    file2.close()   
#-------------------------------------------------------------------------------
def join_i():
    """
        pre: deux fichiers contenant un certain nombre de lignes.
        post: La lettre -i permet de faire en sorte que même si le premier champs commence avec une majuscule ou une minuscule ils peuvent quand même être joint
    """
    file = open('test5.txt', 'r')
    file2 = open('test6.txt', 'r')
    data = file.readlines()
    data2 = file2.readlines()
    for i in range(len(data)):
        data[i] = data[i].strip('\n').split()
        data2[i] = data2[i].strip('\n').split()
        if data[i][0].upper() == data2[i][0].upper():
            print("{} {} {}".format(data[i][0]," ".join(data[i][1:])," ".join(data2[i][1:])))
    file.close()
    file2.close()  

#------------------------------------------------------------------------------
def join_12(x,y):
    """
        pre: Deux fichiers contenant un certain nombre de lignes. X et Y sont des nombres.
        post: Les deux fichiers vont être joints si le champs x du fichier 1 et le y du fichier 2 s'ils ont identiques. 
    """
    file = open('test7.txt', 'r')
    file2 = open('test8.txt', 'r')
    data = file.readlines()
    data2 = file2.readlines()
    for i in range(len(data)):
        data[i] = data[i].strip('\n').split()
        data2[i] = data2[i].strip('\n').split()
        if data[i][0] == data2[i][0]:
            if data[i][x-1] == data2[i][y-1]:
                print("{} {} {} {} {}".format(data[i][x-1],"".join(data[i][:x-1])," ".join(data[i][x:]),"".join(data2[i][:x-1])," ".join(data2[i][x:])))
    file.close()
    file2.close()

join()
print('-----------------------')
join_t(n)
print('-----------------------')
join_i()
print('-----------------------')
join_12(1,1)
