import argparse

parser = argparse.ArgumentParser()
parser.add_argument("FILE", type=str, help="File where are the lines")
parser.add_argument("-k", type=int, help="Sort via the field K as a key")
parser.add_argument("-s", type=str, help="Stabilize sort by disabling last - resort comparison")
parser.add_argument("-t", type=str, help="Use SEP instead of non - blank to blank transition to separate fields")
args = parser.parse_args()


def sort(FILE, k=None, s=None, t = None):
    """
        pre : reçoit en argument un fichier
        post : return le fichier trié.
         Si - k, le fichier sera trié par rapport à la key K.
         Si - s, le fichier sera trié en utilisant la comparaison comme dernière solution
         Si -t, le fichier sera trié en séparant les termes par le caractère précisé (exemple : ",")
    """
    with open(FILE, "r") as f:
        dossier = f.readlines()
        
    if k == None and s == None and t == None:
        lst =[]
        for line in dossier:
            lst.append(line)
        lst.sort()
        return lst

  
    if k != None and s == None:
        tableau = []
        tableau_a_trier = []
        nbre = 0
        count = 0
        count_2 =0
        nbre_actuel = 1
        tableau_final = []
        for line in dossier:
            key = line.split(" ")
            tableau.append(key)
            nbre +=1
            if k > len(key):
                return ("valeur de k inexistante")
        while nbre_actuel <= nbre:
            tableau_a_trier.append(tableau[count][k])
            count +=1
            nbre_actuel +=1
        tableau_a_trier.sort()
        try:
            while count_2 != len(tableau_a_trier):
                for n in tableau:
                    if n[k] == tableau_a_trier[0]:
                        tableau_final.append(n)
                        del tableau_a_trier[0]
                        count_2 +=1
        except :
            print(tableau_final)
            with open(FILE, 'w') as f:
                for n in tableau:
                    f.writelines(n)
            return(tableau_final)
        
    if t != None :
        tableau = []
        for line in dossier:
            key = line.split(t)
            tableau.append(key)
        tableau.sort()
        print (tableau)
        with open(FILE, 'w') as f:
            for n in tableau:
                f.writelines(n)
        return tableau
            
    if k != None and s != None:
        tableau = []
        tableau_a_trier = []
        nbre = 0
        count = 0
        count_2 =0
        nbre_actuel = 1
        tableau_final = []
        for line in dossier:
            key = line.split(" ")
            tableau.append(key)
            nbre +=1
            if k > len(key):
                return ("valeur de k inexistante")
        while nbre_actuel <= nbre:
            tableau_a_trier.append(tableau[count][k])
            count +=1
            nbre_actuel +=1
        tableau_a_trier.sort()
        try:
            while count_2 != len(tableau_a_trier):
                for n in tableau:
                    if n[k] == tableau_a_trier[0]:
                        tableau_final.append(n)
                        del tableau_a_trier[0]
                        count_2 +=1
        except :
            print(tableau_final)
            with open(FILE, 'w') as f:
                for n in tableau:
                    f.writelines(n)
            return(tableau_final)
    


if __name__ == '__main__':
        sort(args.FILE, args.k, args.s, args.t)

