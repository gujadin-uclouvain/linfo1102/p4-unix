import argparse
import random


# On pose les différents arguments possibles
# Ceux avec - sont facultatifs
parser = argparse.ArgumentParser()
parser.add_argument("FILE", type=str, help="File where are the lines")
parser.add_argument("-n", type=int, help="Print COUNT lines picked in FILE")
parser.add_argument("-o", type=str, help="Shuffle and write lines from FILE in FILE2")
parser.add_argument("-r", action='store_true', help="Print lines picked in FILE")
args = parser.parse_args()


def shuf(FILE, n=None, o=None):
    """
    Function needed if -r is not specified
    Only picks once each line at most
    It prints or write in a file lines according to the passed arguments -n and -o
    -n gives a number of lines (int)
    -o gives a file name (str)
    """
    with open(FILE, "r") as f:
        content = f.readlines()
    random.shuffle(content)
    
    if n != None and n < 0:
        n = 0
    
    if n == None and o == None:
        for line in content:
            print(line, end ='')
        return
    
    elif n != None and o == None:
        if len(content) > n:
            for line in content[:n]:
                print(line, end ='')
            return
        elif len(content) <= n:
            for line in content:
                print(line, end ='')
            return
    
    elif n == None and o != None:
        with open(o, 'w') as f:
            f.writelines(content)
        return
    
    elif n != None and o != None:
        if len(content) > n:
            with open(o, 'w') as f:
                f.writelines(content[:n])
            return
        elif len(content) <= n:
            with open(o, 'w') as f:
                f.writelines(content)
            return

def shuf_R(FILE, n=None, o=None):
    """
    Function needed if -r is specified
    May picks several times the same line
    It prints or write in a file lines according to passed arguments -n and -o
    -n gives a number of lines (int)
    -o gives a file name (str)
    """
    with open(FILE, 'r') as f:
        content = f.readlines()
        content[-1] = content[-1]
        
    if n != None and n < 0:
        n = 0
        
    if n==None and o==None:
        try:
            while True:
                rand_num = random.randint(0, len(content)-1)
                print(content[rand_num], end ='')
        except:
            return
    
    elif n != None and o == None:
        counter = 0
        while counter < n:
            rand_num = random.randint(0, len(content)-1)
            print(content[rand_num], end ='')
            counter += 1
        return
            
    elif o != None and n!= None:
        lst = []
        counter = 0
        while counter < n:
            rand_num = random.randint(0, len(content)-1)
            lst.append(content[rand_num])
            counter += 1
        with open(o, 'w') as f:
            lst[-1] = lst[-1][:-1]
            f.writelines(lst)
            return 

if __name__ == '__main__':
    if args.r == True:
        shuf_R(args.FILE, args.n, args.o)
    else:
        shuf(args.FILE, args.n, args.o)
