import argparse


# On pose les différents arguments possibles
# Ceux avec - sont facultatifs
parser = argparse.ArgumentParser()
parser.add_argument("FILE", type=str, help="File where are the lines")
parser.add_argument("-c", action='store_true', help="prefix lines by the number of occurrences")
parser.add_argument("-d", action='store_true', help="only print duplicate lines, one for each group")
parser.add_argument("-s", type=int, help="avoid comparing the first N characters")
parser.add_argument("-u", action='store_true', help="only print unique lines")
parser.add_argument("-w", type=int, help="compare no more than N characters in lines")
parser.add_argument("N", type=int, help="Number of character")
args = parser.parse_args()


def uniq(FILE, N, c=None, d=None, s=None, u=None, w=None):
    """
    @pre:
    When uniq is needed
    @post:
    It prints lines in a file according to the passed arguments -c, -d, -s, -u and -w
    -c prints the number of occurence of a line
    -d prints only the duplicated lines
    -s compares the lines except the N firsts characters
    -u prints only the uniques lines
    -w compares no more than N characters in a line
    """
    dic = {}
    with open (args.FILE[0],"r") as f:
        for line in f: 
            if line in dic:
                dic[line] += 1
            else:
                dic[line] = 1
        content = f.readlines()
    if c != None and d == None and s == None and u == None and w == None:
        count = 1
        for line in content:
            counter = 1
            try:
                if line != content[count]:
                    print(counter,line)
                else:
                    counter+=1
                    print(counter,line)
                    counter = 1  
                count+=1
            except:
                print(counter,line)
        return
    
    elif c == None and d != None and s == None and u == None and w == None:
        string=""
        for line,count in dic.items():
            if count > 1:
                string += "{0}".format(line)
        print(string)
        return
        
    elif c == None and d == None and s != None and u == None and w == None:
        num = args.s
        lst = []
        for line,number in dic.items():
            lst.append(line)       
        new_lst = []
        new_lst.append(lst[0])
        for a in lst[1:]:
            boo = True
            for x in new_lst:
                if a[num:] == x[num:]:
                    booleen=False
            if booleen:
                new_lst.append(a)
        string = "" 
        for b in new_lst:
            string+="{0}".format(b)
        print(string)
        return
    
    elif c == None and d == None and s == None and u != None and w == None:
        string=""
        for line,num in dic.items():
            if num == 1:
                string += "{0}".format(line)
        print(string)
        return
    
    elif c == None and d == None and s == None and u == None and w != None:
        num = args.w
        lst = []
        for line,number in dic.items():
            lst.append(line)          
        new_lst = []
        new_lst.append(lst[0])
        for fact in lst[1:]:
            booleen = True
            for x in new_lst:
                if fact[:num] == x[:num]:
                    booleen = False
            if booleen:
                new_lst.append(fact)             
        string = ""
        for a in new_lst:
            string+="{0}".format(a)
        print(string)
        
    elif c == None and d == None and s == None and u == None and w == None:    
        for line in content:
            print(line)
        return


if __name__ == '__main__':
    uniq(args.FILE, args.c, args.d, args.s, args.u, args.w)