import argparse

parser = argparse.ArgumentParser(description="Join")
parser.add_argument('-i', '--ignore_case', action = "store_true", default=False, help='Ignore les differences entre les majuscules et minuscules des fields')
parser.add_argument('-t', '--CHAR', metavar='caract qui sépare les field', type=str, default=" ", help='Choisis la séparation entre les fields')
parser.add_argument('-1', '--FIELD', metavar='Numero du field du fichier 1', type=int, default=1, help='Choisis le field sur lequel join dans le fichier 1')
parser.add_argument('-2', '--FIELD2', metavar='Numéro du field du fichier 2', type=int, default=1, help='Choisis le field sur lequel join dans le fichier 2')
parser.add_argument('fichier1', metavar='fichier1', type=str, help='Le fichier Text 1 à join')
parser.add_argument('fichier2', metavar='fichier2', type=str, help='Le fichier  Text 2 à join.')      # initialisation des arguments de Join


args = parser.parse_args()

with open(args.fichier1, "r") as F1:
    lignes1 = F1.readlines()
with open(args.fichier2, "r") as F2:    # J'ouvre les fichiers fournis par la commande et je sauvegarde toutes les lignes des 2 fichiers
    lignes2 = F2.readlines()
    
    
for i in range(len(lignes1)):
    lignes1[i] = lignes1[i].strip()
    if args.ignore_case:               # ici je réorganise les lignes pour les comparer plus facilement
        lignes1[i] = lignes1[i].lower()
    lignes1[i] = lignes1[i].split(args.CHAR)  # le split va varier en fonction du -t qu'on veut, par delfaut il est initialisé à un espace : " "
                     
for j in range(len(lignes2)):
    lignes2[j] = lignes2[j].strip()
    if args.ignore_case:                  # même chose pour le 2ème fichier
        lignes2[j] = lignes2[j].lower()
    lignes2[j] = lignes2[j].split(args.CHAR)
    

for i in range(len(lignes1)):        # dans la première boucle, je compare la premiere ligne de fichier 1 avec la premiere du fichier 2
	try:
		if lignes1[i][args.FIELD - 1] == lignes2[i][args.FIELD2 - 1]:   # si le field demander correspond bien au deuxième field du fichier 2,
			print(lignes1[i][args.FIELD - 1],end=args.CHAR)
			for mots in lignes1[i]:
				if mots != lignes1[i][args.FIELD - 1]:
					print(mots,end=args.CHAR)   # j'imprime : le field, le reste du fichier 1
			for mots in lignes2[i]:
				if mots != lignes2[i][args.FIELD2 - 1]:  # puis le reste du fichier 2 sans son field
					print(mots,end=args.CHAR)
			print(" ")   # retour à la ligne
	except IndexError:
		pass   # ici le try et except sert si le field demander est "out of range" par exemple si on cherche à join sur le field 4
	               # et qu'à la premiere ligne on à que 2 field mais que dans la deuxieme ligne on en a 5, on passe la première ligne mais pas la deuxième. 

    


