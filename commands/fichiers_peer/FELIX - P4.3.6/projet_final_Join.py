import argparse
import sys

###Partie génération de la commande et ajout des arguments###
parser = argparse.ArgumentParser(description=("Join 2 files. Both files must be sorted !"))
parser.add_argument('-i', '--ignorecase', action="store_true", help='ignore differences in case when comparing fields')
parser.add_argument('-t', metavar='CHAR',type=str, help='use CHAR as input and output field separator')
parser.add_argument('-1', '--first', metavar='FIELD', type=int, help='join on this FIELD of file 1')
parser.add_argument('-2', '--second', metavar='FIELD', type=int, help='join on this FIELD of file 2')
parser.add_argument('file1', type=str)
parser.add_argument('file2', type=str)
args = parser.parse_args()

###Création du string qui sert de réponse###
str = ''

###Lecture des 2 fichiers###
try:
    with open(args.file1, 'r') as file:
        first = file.readlines()
    with open(args.file2, 'r') as file:
        second = file.readlines()
except: #Affiche un message si un/les fichier(s) sont introuvables et arrête le programme
    print("File(s) not found") 
    sys.exit()
if len(first) == 0 or len(second) == 0: #Affiche un message si un/les fichier(s) sont vides
    print("EMPTY FILE(S)")
    parser.print_usage()

###Parcours chaque ligne des fichiers###
for i in range(min(len(first), len(second))):
    #On stock la ligne dans une liste mots par mots (ex: temp) ou la ligne entière (ex: tempbis)
    temp = first[i].strip().split()
    tempbis = first[i].strip()
    temp2 = second[i].strip().split()
    temp2bis = second[i].strip()
    #Cas argument = -i
    if args.ignorecase:
        if temp[0].lower() == temp2[0].lower():
            str += tempbis
            str += temp2bis[len(temp2[0]):]
            str += "\n"
    #Cas arguments = -1 -2 -t
    elif args.first and args.second and args.t:
        #Split la ligne en fonction d'un argument et non plus un espace
        temp = first[i].strip().split(args.t)
        tempbis = first[i].strip(args.t)
        temp2 = second[i].strip().split(args.t)
        try: 
            if temp[args.first-1] == temp2[args.second-1]:
                str += temp[args.first-1]
                temp.pop(args.first-1)
                for a in temp:
                    str += args.t + a
                temp2.pop(args.second-1)
                for u in temp2:
                    str += args.t + u
                str += "\n"
        #Si le caractère n'est pas dans les textes OU Si on donne une colonne qui n'existe pas, on envoi un message
        except IndexError:
            print("The specified CHAR is not in the texts\nOR\nFIELD out of range")
            sys.exit()
    #Cas argument = -t
    elif args.t:
        try:
            if temp[0] == temp2[0] and args.t in tempbis:             
                str += tempbis
                str += args.t + temp2bis[len(temp2[0]):]
                str += "\n"
        #Si le caractère n'est pas dans les textes, on envoi un message
        except IndexError:
            print("The specified CHAR is not in the texts")
            sys.exit()
    #Cas arguments = -1 -2 
    elif args.first and args.second:
        try:
            if temp[args.first-1] == temp2[args.second-1]:
                str += temp[args.first-1]
                temp.pop(args.first-1)
                for a in temp:
                    str += " " + a
                temp2.pop(args.second-1)
                for u in range(len(temp2)):
                    str += " " + temp2[u]
                str += "\n"
        #Si on donne une colonne qui n'existe pas, on envoi un message
        except IndexError:
            print("FIELD out of range")
            sys.exit()
    #Cas arguments = -1
    elif args.first:
        try:   
            if temp[args.first-1] == temp2[0]:
                str += temp[args.first-1]
                temp.pop(args.first-1)
                for a in temp:
                    str += " " + a
                for u in range(1, len(temp2)):
                    str += " " + temp2[u]
                str += "\n"
        #Si on donne une colonne qui n'existe pas, on envoi un message
        except IndexError:
            print("FIELD out of range")
            sys.exit()
    #Cas arguments = -2
    elif args.second:
        try:
            if temp[0] == temp2[args.second-1]:
                str += tempbis
                for u in range(1, len(temp2)):
                    str += " " + temp2[u]
                str += "\n"
        #Si on donne une colonne qui n'existe pas, on envoi un message
        except IndexError:
            print("FIELD out of range")
            sys.exit()
    #Si aucun argument n'est mentionne, retourne les détails de la commande
    else:
        parser.print_usage()
        
print(str)