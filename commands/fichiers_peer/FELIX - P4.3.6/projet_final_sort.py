# utilitaire sort
# Jérémie Kuperblum 

import argparse
import sys 
from operator import itemgetter

parser = argparse.ArgumentParser(description='sort lines of text files')
parser.add_argument('-k', '--key', metavar='Numero de colonne', type=int, help = 'sort via a key')
parser.add_argument('-s', '--stable', action ="store_true", help = 'stabilize sort by disabling last-resort comparison')
parser.add_argument('-t', '--field_separator', metavar = 'caractère', type=str, help='use SEP instead of non-blank to blank transition')
parser.add_argument('fichier', metavar = 'fichier', type = str, nargs = '+', help = 'Le fichier où se trouvent les données.')



    
args = parser.parse_args(file)


for i, fichier in enumerate(args.fichier):
    if len(args.fichier) > 1:
        if i > 0:
            print()
        print('==>', fichier, '<==')
    try :
        with open (fichier, 'r') as f:
            listlines = f.readlines() # on créer un eliste avec comme élément les lignes du fichier
    except :
        print ("fichier introuvable")
        sys.exit() 
        
    ##### -t et -k et -s #####
    #  conditions :
    # 1) -t :
    #   - le carractère séparateur de colone doit être présent dans toutes les lignes du fichier.
    
    # 2) -k :
    #   - le nombre de colonne de chque ligne dans le fichier doit être au minimum égale au
    #     numero de colone spécifier en argument qui définie sur quelle colonne on doit faire le tri.
    
    # 3) -s :
    #   - aucune condition
    
    # 4) De manière générale :
    #   - Lorsque que l'on compare les lignes pour les trier, un espace sera considérer plus petit
    #     que tout autre caractère.
    
    if args.field_separator and args.key and args.stable :
        try :
            for i in range (len(listlines)) :
                listlines[i] =  listlines[i].strip().split(args.field_separator) # on split en fonction du carractère donner argument de -t
        except :
            print ("Toutes les ligne ne contiennent pas le caractère spécifié en argument de -t")
            sys.exit()
        try :
            for i in range (len(listlines)):
                listlines[i][0:0] = [listlines[i][args.key-1]] # on prend l'élément en position corespondant au chiffre donner en argument de -k et on le met devant dans la liste.
                for j in range (args.key, len(listlines[0])):
                    listlines[i][0] += listlines[i][j] # on rajoute au premier élément de la liste les éléments qui suit la position donner en argument -k.
        except :
            print ("index out of range, toutes le ligne du fichier n'ont pas au minimum un nombre de colonne égale à l'argument spécifier de -k")
            sys.exit()
        listlines = sorted(listlines, key=itemgetter(0)) # on trie en fonction du premier élément de chaque listes.
        for i in range (len(listlines)):
            listlines[i] = listlines[i][1:] # on supprime le premier élément de chaque lignes.
        for i in range(len(listlines)) :
            listlines[i] = "{}".join(listlines[i]).format (args.field_separator) # on join chaque éléments de chaque listes pour optenir un string. 
        listlines = "\n".join(listlines) # on join les listes.
        print (listlines)

        
    ##### -k et -t #####
    #  conditions :
    
    # 1) -t :
    #   - le carractère séparateur de colone doit être présent dans toutes les lignes du fichier.
    
    # 2) -k :
    #   - le nombre de colonne de chque ligne dans le fichier doit être au minimum égale au
    #     numero de colone spécifier en argument qui définie sur quelle colonne on doit faire le tri.
    
    # 3) De manière générale :
    #   - Lorsque que l'on compare les lignes pour les trier, un espace sera considérer plus petit
    #     que tout autre caractère.

    elif args.key and args.field_separator :
        try :
            for i in range (len(listlines)) :
                listlines[i] =  listlines[i].strip().split(args.field_separator) # on split en fonction du carractère donner argument de -t.
        except :
            print ("Toutes les ligne ne contiennent pas le caractère spécifié en argument de -t")
            sys.exit()
        try :
            for i in range (len(listlines)):
                listlines[i][0:0] = [listlines[i][args.key-1]] # on prend l'élément en position corespondant au chiffre donner en argument de -k et on le met devant dans la liste.
                if args.key > 1:
                    for j in range (args.key-1, 0 , -1):
                        listlines[i][0] += listlines[i][j] # on rajoute tout les élément en position qui précédait la position donner en argument de -k. L'ajout des éléments ce fait en position décroisant.
        except :
                print ("index out of range, toutes le ligne du fichier n'ont pas au minimum un nombre de colonne égale à l'argument spécifier de -k")
                sys.exit()
        listlines = sorted(listlines, key=itemgetter(0)) # on trie en fonction du premier élément de chaque listes.
        for i in range (len(listlines)):
            listlines[i] = listlines[i][1:]
        for i in range(len(listlines)) :
            listlines[i] = "{}".join(listlines[i]).format(args.field_separator) # on join chaque éléments de chaque listes pour optenir un string et en join en fonction du caractère donné en argument de -t.
        listlines = "\n".join(listlines) # on join les listes.
        print (listlines)

    
    ##### -k et -s #####
    #  conditions :
    
    # 1) -k :
    #   - le nombre de colonne de chque ligne dans le fichier doit être au minimum égale au
    #     numero de colone spécifier en argument qui définie sur quelle colonne on doit faire le tri.
    
    # 2) -s :
    #   - aucune condition
    
    # 3) De manière générale :
    #   - Lorsque que l'on compare les lignes pour les trier, un espace sera considérer plus petit
    #     que tout autre caractère.
    elif args.key and args.stable :   
        for i in range (len(listlines)) :
            listlines[i] =  listlines[i].split() # on split les listes.
        try :
            for i in range (len(listlines)):
                listlines[i][0:0] = [listlines[i][args.key-1]]# on prend l'élément en position corespondant au chiffre donner en argument de -k et on le met devant dans la liste.
                if args.key > 1 :
                    for j in range (args.key, len(listlines)):
                        listlines[i][0] += listlines[i][j] # on rajoute au premier élément de la liste les éléments qui suit la position donner en argument -k.
        except :
            print ("index out of range, toutes le ligne du fichier n'ont pas au minimum un nombre de colonne égale à l'argument spécifier de -k")
            sys.exit()
        listlines = sorted(listlines, key=itemgetter(0))# on trie en fonction du premier élément de chaque listes.
        for i in range (len(listlines)):
            listlines[i] = listlines[i][1:]
        for i in range(len(listlines)) :
            listlines[i] = " ".join(listlines[i]) # on join chaque éléments de chaque listes pour optenir un string.
        listlines = "\n".join(listlines) # on join les listes.
        print (listlines)

        
    ##### -k #####
    #  conditions :
    
    # 1) -k :
    #   - le nombre de colonne de chque ligne dans le fichier doit être au minimum égale au
    #     numero de colone spécifier en argument qui définie sur quelle colonne on doit faire le tri.
    elif args.key :  
        for i in range (len(listlines)) :
            listlines[i] =  listlines[i].split() # on split les listes.
        try :
            for i in range (len(listlines)):
                listlines[i][0:0] = [listlines[i][args.key-1]] # on prend l'élément en position corespondant au chiffre donner en argument de -k et on le met devant dans la liste.
                if args.key > 1 :
                    for j in range (args.key-2, -1 , -1):
                        listlines[i][0] += listlines[i][j] # on rajoute tout les élément en position qui précédait la position donner en argument de -k. L'ajout des éléments ce fait en position décroisant.
        except :
            print ("index out of range, toutes le ligne du fichier n'ont pas au minimum un nombre de colonne égale à l'argument spécifier de -k")
            sys.exit()
        listlines = sorted(listlines, key=itemgetter(0))# on trie en fonction du premier élément de chaque listes.
        for i in range (len(listlines)):
            listlines[i] = listlines[i][1:]
        for i in range(len(listlines)) :
            listlines[i] = " ".join(listlines[i]) # on join chaque éléments de chaque listes pour optenir un string.
        listlines = "\n".join(listlines)# on join les listes.
        print (listlines)

    
    ##### -s #####
    #  conditions :
    
    # 1) -s :
    #   - aucune condition
    
    # 2) De manière générale :
    #   - Lorsque que l'on compare les lignes pour les trier, un espace sera considérer plus petit
    #     que tout autre caractère.
    elif args.stable :  
        for i in range (len(listlines)):
            listlines[i] = listlines[i].strip() # on split les listes.
        listlines = sorted (listlines) # on trie les listes 
        listlines = "\n".join(listlines) # on join les listes.
        print (listlines)


    ##### -t #####
    #  conditions :
    
    # 1) -t :
    #   - aucun condition
    
    # 2) De manière générale :
    #   - Lorsque que l'on compare les lignes pour les trier, un espace sera considérer plus petit
    #     que tout autre caractère.
    elif args.field_separator :   
        for i in range (len(listlines)):
            listlines[i] = listlines[i].strip() # on split les listes.
        listlines = sorted (listlines) # on trie les listes 
        listlines = "\n".join(listlines) # on join les listes.
        print (listlines)

        
    ##### imprime le text si il n'y a aucune commande #####
    else:
        for l in listlines:
            print(l.strip())
        
if __name__ == '__main__':
    unittest.main()
    

