#!/usr/bin/env python3
# -*- coding: utf-8 -*
import argparse
import os
import sys


def make_dic(file):
    """
    Pre: file est un path vers un fichier txt qui existe
    Post: retourne un dictionnaire {ligne: [numero de la ligne de la première occurence, le nombre d'occurences]}
    """
    lines = {}
    if not os.path.isfile(file):
        print("Le chemin vers le fichier/le fichier n'existe pas")
        sys.exit()
    else:
        with open(file, "r") as f:
            f_lines = f.readlines()
            for i in range(len(f_lines)):
                # Enleve les caractères spéciaux. line correspond à une ligne du fichier
                line = f_lines[i].strip()
                if line in lines.keys():
                    # Si la ligne déja dans lines ajoute 1 aux occurences de la ligne
                    lines[line][1] += 1
                else:
                    # rajoute line dans lines, initialise la ligne de la 1re occu à i et initialise les occu à 1
                    lines[line] = [i, 1]
    return lines


def get_output(l):
    """
    Pre: l is a list [line,pos_in_file] l est une list [ligne, position dans le fichier txt]
    Post: retourne un string avec les lignes triées
    """
    output = ""
    sorted(l, key=lambda line: line[1])  # Trie la liste selon le deuxième élément de la list (cf. Pré/Post)
    for line in l:
        output += line[0] + "\n"
    return output[:-1]


def default(file):
    """
    Pre: file est un path qui renvoi vers un fichier qui existe
    Post: retourne les lignes sans duplicata dans un string
    """

    lines = make_dic(file)
    l = []
    for key in lines.keys():
        l.append([key, lines[key][1]])
    return get_output(l)


def unique(file):
    """
    Pre: file est un path qui renvoi vers un fichier qui existe
    Post: retourne uniquement les lignes qui n'ont qu'une seule occurence dans un string
    """
    lines = make_dic(file)
    l = []
    for key in lines.keys():
        if lines[key][1] == 1:
            l.append([key, lines[key][0]])
    return get_output(l)


def repeated(file):
    lines = make_dic(file)
    l = []
    for key in lines.keys():
        if lines[key][1] > 1:
            l.append([key, lines[key][0]])
    return get_output(l)


def skip(file, car):
    """
    Pre: file est un path vers un fichier txt qui existe
         car est le nombre de caractères qu'il faut ignorer lors de la vérification
    Post: retourne sans duplicata(en ignorant les 'car' premiers caractères de la ligne) les lignes de file dans un string
    """
    lines = {}
    output = ""
    with open(file, 'r') as f:
        for line in f.readlines():
            if line[car+1:] not in lines.keys():
                output += line
                lines[line[car+1:].strip("\n")] = 1
            else:
                lines[line[car+1:].strip("\n")] += 1
    return output


def check(file, car):
    """
    Pre: file est un path vers un fichier txt qui existe
         car est le nombre de caractères à vérifier
    Post: retourne dont les lignes avec les 'car' premiers caractères les mêmes sont repris qu'une seule fois dans un string
    """
    lines = {}
    output = ""
    with open(file, 'r') as f:
        for line in f.readlines():
            if line[:car] not in lines.keys():
                output += line
                lines[line[:car].strip('\n')] = 1
            else:
                lines[line[:car].strip('\n')] += 1
    return output


def count(file):
    """
    Pre: file est un path vers un fichier txt qui existe
    Post: retourne dans un string 'nbr_occurences ligne' pour toutes les lignes sans les duplicatas
    """
    lines = make_dic(file)
    # makes a list ["occurence line", nbr of the first line]
    l = [["{} {}".format(lines[line][1], line), lines[line][0]] for line in lines.keys()]
    return get_output(l)


parser = argparse.ArgumentParser(description="N'affiche pas les duplicatas de lignes.")
parser.add_argument('-u', '--unique', action='store_true', help="N'affche que les lignes uniques")
parser.add_argument('-d', '--repeated', action='store_true', help="N'afficher que les lignes dupliquées")
parser.add_argument('-s', '--skip', type=int, default=1, help='[-c n] Ignore le nombre caractères n au debut de la ligne')
parser.add_argument('-w', '--check', type=int, default=1, help='[-w n] compare que le nombre de caractère n au debut de la ligne')
parser.add_argument('-c', '--count', action='store_true', help="Afficher également le nombre d'occurence de  chaque ligne")
parser.add_argument('fichier', metavar='fichier', type=str, nargs='+', help='Le fichier à verifier.')

args = parser.parse_args()

for i, fichier in enumerate(args.fichier):
    if args.unique:
        print(unique(fichier))
    elif args.repeated:
        print(repeated(fichier))
    elif args.skip:
        print(skip(fichier, args.skip))
    elif args.check:
        print(check(fichier, args.check))
    elif args.count:
        print(count(fichier), end='')
    else:
        print(default(fichier), end='')
