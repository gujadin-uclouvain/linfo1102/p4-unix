# METHODE SHUF
# Swierzewski Cezary

import argparse
import random

parser = argparse.ArgumentParser(description = "Générer des permutations aléatoires. ")
parser.add_argument('-n', '--lines', metavar = 'N', type = int , help = 'Afficher N ligne(s) en ordre aléatoire.')
parser.add_argument('-r', '--repeat', action = 'store_true', help = '''Afficher les lignes de façon aléatoire à l'infinie.''')
parser.add_argument('-o', '--file', metavar = 'O', type = str , help = ' Afficher les lignes de façon aléatoire dans un fichier externe (O = le fichier). ')
parser.add_argument('fichier', metavar = 'fichier', type = str, nargs = '+', help = 'Le fichier où se trouvent les données.')

  
args = parser.parse_args() 
for i, fichier in enumerate(args.fichier): # boucle qui éxecute le code pour chaque fichier si plusieurs 
    if len(args.fichier) > 1:
        if i > 0:
            print()
        print('==>', fichier, '<==')
    with open(fichier, 'r') as f:
        data = f.readlines()
        random.shuffle(data) # shuffle la liste 
        if args.lines: # option -n, affiche n ligne(s) en ordre aléatoire 
            for l in data[:args.lines]:
                print(l.strip())
        elif args.repeat: # option -r, répete les lignes de façon aléatoire à l'infinie 
            while True:
                for l in data:
                    print(l.strip())
                random.shuffle(data)
        elif args.file: # option -o, shuffle les lignes dans un fichier externe 
            with open(args.file, 'w') as fw:
                for l in data:
                    fw.write(l)
        else: # option de base
            for l in data:
                print(l.strip())


