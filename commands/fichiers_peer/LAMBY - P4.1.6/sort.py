#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser(description="Trie les lignes de un ou plusieurs fichiers par ordre croissant.")
parser.add_argument('-k', '--key', metavar='K', type=int, default=0, help="Trie selon l'indice 'k', le tri'effectue selon le kième élément de la chaine de caractère.")
parser.add_argument('-s', '--stable', action="store_true", help="Trie par ordre d'apparition dans le/les fichier(s) en cas d'égalité.")
parser.add_argument('-t', '--fieldseparator', metavar='SEP', type=str, help="Sépare les mots/nombres en colonnes avec le caractère spécifié.")
parser.add_argument('fichier', metavar='fichier', type=str, nargs='+', help='Le fichier à trier.')

args = parser.parse_args()



def get_lines():
    """
    """
    
    allLines = []
    for i, fichier in enumerate(args.fichier):
        with open (fichier) as file:
            for line in file.readlines():
                allLines.append(line.strip())
    return allLines



def sortUnix():
    """
    """
    
    allLines = get_lines()
    try:
        allLines = sorted(allLines, key=lambda v: (v.upper(), v[0].isupper()))
    except:
        allLines = sorted(allLines)

    sort_print(allLines)
        
        

def sort_k():
    """
    """
    
    allLines = get_lines()
    allLines2 = []
    allLines1 = []


    # Divise les lignes avec moins de k colonnes et les autres
    
    for i in range(len(allLines)):
        allLines[i] = allLines[i].split()
    for j in range(len(allLines)):
        if len(allLines[j]) < args.key:
            allLines2.append(allLines[j])
        else:
            allLines1.append(allLines[j])
    
    
    # Trie les lignes avec k colonnes
    
    allLines1 = sorted(allLines1, key=lambda x:x[args.key - 1])
    allLines = allLines2 + allLines1
    
    sort_print(allLines)



def sort_s():
    """
    Pas complet, pour le moment, fait la même
    chose de sortUnix()
    """
    
    allLines = get_lines()
    sort_print(allLines)
        
        
        
def sort_t():
    """
    """
    
    allLines = get_lines()
    
    # Séparer les colonnes avec le signe
    
    for i in range(len(allLines)):
        allLines[i] = allLines[i].split(args.fieldseparator)
    
    # Trier
    
    try:
        allLines = sorted(allLines, key=lambda v: (v.upper(), v[0].isupper()))
    except:
        allLines = sorted(allLines)
    
    # Phase d'écriture de '-t'
    
    for i in range(len(allLines)):
        allLines[i] = args.fieldseparator.join(allLines[i])
        print(allLines[i])
        
        

def sort_print(linesTab):
    """
    """
    
    for i in range(len(linesTab)):
        linesTab[i] = "".join(linesTab[i])
        print(linesTab[i])
        
        
        
if args.key < 1 and not args.stable and not args.fieldseparator:
    sortUnix()

if args.key >= 1:
    sort_k()

if args.stable:
    sort_s()

if args.fieldseparator:
    sort_t()