Fonctionnalités:

Le code ne passe pas les test
Problème au niveau de l'ouverture du fichier :

-	with open("file") as f: (Ne fonctionne pas car file n'est pas le nom du fichier)

Il faudrait remplacer la ligne et changer la ligne du parser par: 

-	with open(fichier, "r") as f
-	parser.add_argument('fichier', type=str, nargs='+', help='Le fichier à afficher.')

Du coup compliqué de tester le code fonctionnel si le fichier ne peut être lu mais en analysant le code j'ai pu constater un problème:
-il n'y a aucun print des résultats des commandes mais qu'un seul return pour tout ? J'ai l'impression que le code ne gère qu'un seul cas (-c) mais pas le reste



Style de programmation:

-Il y a un manque de structure dans le code on ne comprend pas trop ce qu'il se passe
-Il n'y a pas de commentaires pour aider la compréhensions ni de pré/post à la fonction
-le nom des variables est bien choisis 

Amélioration:

Permettre l'ouverture du fichier pour éxecuter le code
Divisier le code pour chaque attribut (-s,-w,-u,etc) et utiliser des print pour afficher le résultat et non un return


