"""
    @author Valentin Baillieux
    @version 01 mars 2019

    pre : les arguments optionnels -t doit être suivit d'un charactere representant le séparateur
                                      de colonne dans les deux fichier.
                                   -i permet d'ignorer les majuscule dans la comparaison des champs
                                      commun des deux fichiers.
                                   -1 et -2 doivent être suivit par un entier réprésentant le numéro
                                      de la colonne du fichier qui doit être pris comme champs commun.
                                   fichier1, doit être le nom d'un fichier texte dans le répertoire.
                                   fichier2, doit être le nome d'un fichier texte dans le répertoire.
                                   Ces deux dernier arguments sont positionnel.
    post: renvois dans le terminal le cotenu des deux fichier joint en fonction de leur champs commun.

"""
import argparse

def char(s):
    if len(s) > 1: #vérifie que le s en entrée est bien un simple caractère
        #remplacer par: "{} n'est pas un charactere".format(s) si la version de python est inférieur à 3.5
        raise argparse.ArgumentTypeError(f"{s} n'est pas un charactere")
    return s

parser = argparse.ArgumentParser(description="Join deux fichiers par rapport a un champs commun")
parser.add_argument('-t', type=char, help='Change le separateur de colonne')
parser.add_argument('-i', action='store_true', help='Ignore les majuscule')
parser.add_argument('-1','--one', type=int, default=1, help='Definis le champ commun du fichier1')
parser.add_argument('-2','--two', type=int, default=1, help='Definis le champ commun du fichier2')
parser.add_argument('fichier1', metavar='fichier1', type=str, help='Le fichier a joindre.')
parser.add_argument('fichier2', metavar='fichier2', type=str, help='Le fichier a joindre.')

args = parser.parse_args()

with open(args.fichier1) as f1:
    lines1_r = [e.strip() for e in f1.readlines()] # on récupère les lignes du fichier1 sous forme ["x","y"]...etc

with open(args.fichier2) as f2:
    lines2_r = [e.strip() for e in f2.readlines()] # on récupère les lignes du fichier1 sous forme ["x","y"]...etc

"""split les colonnes des fichiers en fonction du séparateur"""

if args.t != None: # on vérifie si l'option t a été employée
    lines1 = [e.split(args.t) for e in lines1_r] # on traite les lignes en fonction du séparateur spécifier par l'option
    lines2 = [e.split(args.t) for e in lines2_r]
else:
    lines1 = [e.split() for e in lines1_r] # on traite les lignes pour avoir chaque élément indépandemment
    lines2 = [e.split() for e in lines2_r] # sous cette forme [['x','y'...etc]...etc]

"""récupère l'indice du champs commun"""

if args.one != None:
    cf1 = (args.one)-1 #pour que le champ commun corresponde à l'indice de la list
else:
    cf1 = 0 #si l'option -1 n'est pas employé la première colonne du fichier est employé comme champs commun

if args.two != None:
    cf2 = (args.two)-1 #idem pour le champ du deuxième fichier
else:
    cf2 = 0  #si l'option -2 n'est pas employé la première colonne du fichier est employé comme champs commun

"""vérifie que l'indice est valide et ensuite que les champs commun sont bien égaux"""

cf_1 = []
cf_2 = []
if cf1 > len(lines1[0])-1 or cf2 > len(lines2[0])-1: #vérification de la validité des opt -1 et -2
    raise IndexError("votre champs commun est plus grand que le nombre de colonne du fichier")
else:
    for e in lines1:
        cf_1.append(e[cf1].strip()) #on isole les champs communs pour vérifier qu'ils sont bien égaux
    for e in lines2:
        cf_2.append(e[cf2].strip())

if args.i == True:
    cf_m1 = [e.lower().strip() for e in cf_1] #on réduit le champs en minuscule pour que les champs communs soient égaux
    cf_m2 = [e.lower().strip() for e in cf_2]
    if cf_m1 != cf_m2:
        raise ValueError("vos champs communes ne sont pas égaux malgré l'option i")
else:
    if cf_1 != cf_2:
        raise ValueError('Vos champs communs ne sont pas égaux')

"""Retire le champs commun des deux fichiers pour éviter de le mettre en double dans le résultat final"""

for e in lines1:
    del e[cf1]
for e in lines2:
    del e[cf2]

"""Crée la une liste de listes correspondant aux des fichiers join"""

result = [] #
if args.i == True: #récupère le champs commun sous format correct en fonction de l'option i
    cf_final = []
    for i in range(len(cf_1)):
        if cf_1[i] == cf_1[i].upper() or cf_2[i] == cf_2[i].upper():
            cf_final.append(cf_1[i].upper())
        else:
            cf_final.append(cf_1[i])
else:
    cf_final = cf_1 #ici cf_final = [cf, x, y]

if args.t != None: #on va créer les nouvelles lignes en prenant en compte le séparateur
    sep = args.t
else:
    sep = " "
#on s'occupe des lignes du fichier1
for i in range(len(lines1)): #len(lines1) correspond aux nombres de lignes des fichiers
    el = cf_final[i] + sep # on place le champs commun en premier élément de chaque ligne. Et on rajoute le séparateur
    for e in lines1[i]:
        el += e + sep
    result.append(el)
#on s'occupe des lignes du fichier 2
for i in range(len(lines2)):
    el = result[i] # on récupère la ligne de result
    for e in (lines2[i]):
        if e != lines2[i][len(lines2[i])-1]: # on vérifie que e n'est pas le dernier élément de lines2[i] pour ne pas rajouter un séparateur superflus.
            el += e + sep
        else:
            el += e +"\n"
            result[i] = el
for e in result:
    print(e,end="")
