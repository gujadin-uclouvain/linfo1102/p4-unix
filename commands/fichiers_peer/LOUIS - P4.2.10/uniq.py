# DE GEORGI Hugo

from collections import OrderedDict
import argparse



parser = argparse.ArgumentParser(description="""Filter adjacent matching lines from INPUT (or standard input ) ,
        writing to OUTPUT (or standard output ) .
        With no options , matching lines are merged to the first occurrence .""")
parser.add_argument('-c', '--count', action="store_true", required=False, help=""" prefix lines by the number of occurrences """)
parser.add_argument('-d', '--duplicated', action="store_true", required=False, help=""" only print duplicate lines , one for each group """)
parser.add_argument('-u', '--unique-lines', action="store_true", required=False, help=""" only print unique lines """)
parser.add_argument('-s', '--start-at', metavar='N', type=int,required=False, default=0, help=""" avoid comparing the first N characters """)
parser.add_argument('-w', '--finish-at', metavar='N', type=int,required=False, default=-1, help=""" compare no more than N characters in lines """)
parser.add_argument('fichier', metavar='fichier', type=str, nargs='+',required=True, help='Le fichier à afficher.')

args = parser.parse_args()

def uniq(file, u=False, c=False, d=False, s=0, w=-1): #différents paramètres u, c, d, s, w avec des valeurs par défaut
    with open("file") as f:
        occu = OrderedDict()
        data = sorted(f.readlines())
        out = ""
        for line in data:
            if line not in occu.keys():
                occu.setdefault(line, 1)
            else:
                occu[line] += 1
            data = filter(lambda a: a[s:w] != line[s:w], data)      #uniq -s N et uniq -w N avec comme valeurs par défaut la liste entière
        for key, value in occu.items():
            if u and value > 1 or d and value < 2:  #uniq -u
                continue
            if c:
                out += f"{value} "      #uniq -c (count)
            out += f"{key}"
        return out





