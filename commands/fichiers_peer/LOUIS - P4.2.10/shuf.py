import random
import argparse
parser = argparse.ArgumentParser()

parser.add_argument('-n', '--count', type=int, required=False, help=""" output at most COUNT lines """)
parser.add_argument('-o','--output',type=str,required=False,help=""" write result to FILE instead of standard output """ )
parser.add_argument('-r','--repeat',type=str,required=False,help="""output lines can be repeated""")
parser.add_argument('file1', metavar='file1', type=str, help="""fichier a ouvrir""")
#parser.add_argument('file2', metavar='file2', type=str, help="""fichier ou on va ecrire""")

args = parser.parse_args()

def repeat_with_count(filename,count):  #fonction qui renvoie le nombre de lignes demandées avec chaque ligne full random
    file=open(filename,"r")
    l=file.readlines()
    file.close()
    for y in range(len(l)):
        l[y]=l[y].strip("\n")
    for x in range(count):
        print(random.choice(l))
def repeat_to_infinite(filename):  # fonction qui renvoie une ligne aléatoire a l'infini
    file=open(filename,"r")
    l=file.readlines()
    file.close()
    i=0
    while(True):
        i+=1
        print(l[int(random.random()*len(l))])

def shuf(filename):  #fonction qui mélange toutes les lignes d'un fichier
    file=open(filename,"r")
    l=file.readlines()
    file.close()
    for x in range(len(l)):
        l[x].strip("\n")
    random.shuffle(l)
    file2=open(filename,"w")
    file2.writelines(l)
    file2.close()

def shuf_to_file(input_file,output_file):  #fonction qui mélange toutes les lignes d'un fichier mais les renvoie dans un autre fichier
    file=open(input_file,"r")
    l=file.readlines()
    file.close()
    for x in range(len(l)):
        l[x].strip("\n")
    random.shuffle(l)
    file2=open(output_file,"w")
    file2.writelines(l)
    file2.close()

def count(filename,count):  #fonction qui renvoie le nombre de lignes demandées, et mélangées
    file=open(filename,"r")
    l=file.readlines()
    file.close()
    for x in range(len(l)):
        l[x]=l[x].strip("\n")
    random.shuffle(l)
    file2=open(filename,"w")
    if count>=len(l):
        file2.writelines(l)
    else:
        new_l=l[0:count]
        file2.writelines(new_l)
    file2.close()

def count_to_file(filename1,filename2,count):  #fonction qui renvoie le nombre de lignes demandées et mélangées, dans un autre fichier
    file=open(filename,"r")
    l=file.readlines()
    file.close()
    for x in range(len(l)):
        l[x]=l[x].strip("\n")
    random.shuffle(l)
    file2=open(filename2,"w")
    if count>=len(l):
        file2.writelines(l)
    else:
        new_l=l[0:count]
        file2.writelines(new_l)
    file2.close()


if args.count!=None and args.output==None and args.repeat==None:  #si on appelle uniquement count
    count(args.file1,args.count)

if args.count==None and args.output==None and args.repeat==None:  #si on appelle uniquement l'utilitaire shuf sans spécifications
    shuf(args.file1)

if args.count==None and args.output==None and args.repeat!=None:  #si on appelle uniquement repeat, cela va donc tourner a l'infini
    repeat_to_infinite(args.file1)

if args.count!=None and args.output==None and args.repeat!=None:  #si on appelle count et repeat afin de n'avoir que des valeurs aléatoires
    repeat_with_count(args.file1,args.count)

if args.count==None and args.output!=None and args.repeat==None:  #si on appelle uniquement output de maniere a renvoyer le fichier mélangé dans un autre fichier
    shuf_to_file(args.file1,args.file2)

if args.count!=None and args.output!=None and args.repeat==None:  #si on appelle count mais qu'on veut envoyer le fichier mélangé dans un autre
    count_to_file(args.file1,args.file2,args.count)

if args.count!=None and args.output!=None and args.repeat!=None:  # si on les appelle tous pour n'avoir que des résultats aléatoires et dans un autre fichier
    file=open(args.file1,"r")
    l=file.readlines()
    file.close()
    random.shuffle(l)
    new_l=[]
    for x in range(args.count):
        new_l.append(random.choice(l))
    file2=open(args.file2,"w")
    file2.writelines(new_l)
    file2.close()
"""
if args.count==None and args.output!=None and args.repeat!=None: #car on ne peut pas écrire a l'infini de base, et encore moins dans un fichier
    return "opération impossible"
    """
