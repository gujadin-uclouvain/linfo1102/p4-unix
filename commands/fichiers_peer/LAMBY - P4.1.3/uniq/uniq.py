import argparse
import sys


class Uniq():

    def __init__(self):

        self.parser = argparse.ArgumentParser(
            description="uniq - report or omit repeated lines"
            )
        self.parser.add_argument(
            'infile',
            nargs='?',
            type=argparse.FileType('r'),
            default=sys.stdin)
        self.parser.add_argument(
            '-c',
            '--count',
            dest='c_flag',
            action='store_true',
            help='prefix lines by the number of occurrences')
        self.parser.add_argument(
            '-u',
            '--unique',
            dest='u_flag',
            action='store_true',
            help='only print unique lines')
        self.parser.add_argument(
            '-d', '--repeated',
            dest='d_flag',
            action='store_true',
            help='only print duplicate lines, one for each group')
        self.parser.add_argument(
            '-s',
            '--skip-chars',
            type=int,
            dest='skip_char',
            default=0,
            help='avoid comparing the first N fields')
        self.parser.add_argument(
            '-w',
            '--check-chars',
            type=int,
            dest='check_char',
            default=None,
            help='compare no more than N characters in lines')
        self.args = self.parser.parse_args()

        self.lines = self.read_file()
        self.output = self.unique()

    def read_file(self):
        lines = []
        try:
            for line in self.args.infile:
                lines.append(line)
            return lines
        except FileNotFoundError as e:
            print(f'uniq: {args.infile.name}: No such file or directory')
            sys.exit(-1)

    def unique(self):
        pa = self.args.skip_char
        epa = self.args.check_char
        lines = self.lines
        output = []
        i = 0
        while i < len(lines):
            line = lines[i]
            j = 1
            while i + j < len(lines) and lines[i + j][pa:epa] == line[pa:epa]:
                j += 1
            output.append((j, line))
            i += j

        return output

    def print_output(self):
        for i in self.output:
            if self.args.c_flag:
                print(f'      {i[0]} ', end='')
            if self.args.d_flag:
                if i[0] != 1:
                    print(i[1], end='')
            elif self.args.u_flag:
                if i[0] == 1:
                    print(i[1], end='')
            else:
                print(i[1], end='')


if __name__ == '__main__':
    uniq = Uniq()
    uniq.print_output()
