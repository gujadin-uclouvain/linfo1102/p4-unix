import argparse
import sys
from random import randint
from random import shuffle

class Shuf():

    def __init__(self):

        self.parser = argparse.ArgumentParser(
            description="shuf - generate random permutations"
            )
        self.parser.add_argument(
            'infile',
            nargs='?',
            type=argparse.FileType('r'),
            default=sys.stdin
            )
        self.parser.add_argument(
            '-r',
            '--repeat',
            dest='r_flag',
            action='store_true',
            help='output lines can be repeated'
            )
        self.parser.add_argument(
            '-n',
            '--head-count',
            type=int,
            dest='head_count',
            default=None,
            help='output at most COUNT lines'
            )
        self.parser.add_argument(
            '-o',
            '--output',
            type=argparse.FileType('w'),
            dest='output_file',
            default=sys.stdout,
            help='write result to FILE instead of standard output'
            )
        self.args = self.parser.parse_args()

        self.lines = self.read_file()
        self.output = self.shuf()

    def read_file(self):
        lines = []
        try:
            for line in self.args.infile:
                lines.append(line)
            return lines
        except FileNotFoundError as e:
            #print(f'uniq: {args.infile.name}: No such file or directory')
            sys.exit(-1)

    def shuf(self):
        file = self.lines
        shuffle(file)
        file = file[:self.args.head_count]
        if self.args.r_flag:
            for i in range(randint(0, len(file))):
                file[randint(0, len(file)-1)] = file[randint(0, len(file)-1)]
        return file

    def print_output(self):
        with self.args.output_file as f:
            for i in self.output:
                f.write(i)


if __name__ == '__main__':
    shuf = Shuf()
    shuf.print_output()
