import argparse
import sys


class Sort():

    def __init__(self):

        self.parser = argparse.ArgumentParser(
            description="sort - sort lines of text files"
            )
        self.parser.add_argument(
            'infile',
            nargs='?',
            type=argparse.FileType('r'),
            default=sys.stdin
            )
        self.parser.add_argument(
            '-s',
            '--stable',
            dest='s_flag',
            action='store_true',
            help='stabilize sort by disabling last - resort comparison'
            )
        self.parser.add_argument(
            '-k',
            '--key',
            type=int,
            dest='key',
            default=1,
            help='sort via the field K as a key , K is a field number starting from 1'
            )
        self.parser.add_argument(
            '-t',
            '--field-separator',
            type=str,
            dest='separator',
            default=' ',
            help='use SEP instead of non - blank to blank transition to separate fields'
            )
        
        self.args = self.parser.parse_args()
        self.args.key = self.args.key - 1
        self.lines = self.read_file()
        self.output = self.sort(self.lines)

    def read_file(self):
        lines = []
        try:
            for line in self.args.infile:
                lines.append(line)
            return lines
        except FileNotFoundError as e:
            print(f'uniq: {args.infile.name}: No such file or directory')
            sys.exit(-1)

    def sort(self, input_):
        file = input_
        i = 0
        while i < len(file):
            j = i + 1
            while j < len(file):
                splitted_i = file[i].split(self.args.separator)
                splitted_j = file[j].split(self.args.separator)
                if self.args.key >= len(splitted_i) or self.args.key >= len(splitted_j):
                    self.args.key = 0
                if splitted_i[self.args.key] > splitted_j[self.args.key]:
                    file[i], file[j] = file[j], file[i]
                if splitted_i[self.args.key] == splitted_j[self.args.key] and not self.args.s_flag:
                    next_ = self.args.key + 1 if self.args.key + 1 < len(splitted_i) and self.args.key + 1 < len(splitted_j) else 0
                    if splitted_i[next_] > splitted_j[next_]:
                        file[i], file[j] = file[j], file[i]
                j += 1
            i += 1
        return(file)

    def print_output(self):
        for i in self.output:
            print(i, end='') 


if __name__ == '__main__':
    sort = Sort()
    sort.print_output()
