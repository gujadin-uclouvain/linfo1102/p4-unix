#!/usr/bin/env python3
import argparse

def make_dic(f):
    """
    Pre: f est un fichier.txt trié qu'on analyse
    Post: retourne un dictionnaire contenant la ligne et son occurence ( {ligne: occurence} )
    """
    with open(f, "r") as file:  #On ouvre le fichier qu'on souhaite analyser
        dico={} #initialise dictionnaire
        for line in file: #On passe sur chaque ligne et on ajoute dans le dico en comptant le nombre de répétition
            if line.strip() not in dico:
                dico[line.strip()] = 1
            else:
                dico[line.strip()] += 1
        return dico


def repeated(f):
    """
    pre: f est un fichier.txt trié
    post: retourne une liste (lines) contenant seulement les lignes ayant une occurence strictement supérieure à 1 dans un fichier.txt selon uniq -d
    """
    lines = []
    dico = make_dic(f)
    for line,number in dico.items():
        if number > 1: #Si une ligne se répète plus d'une fois
            lines.append(line)
    return lines

def unique(f):
    """
    pre: f est un fichier.txt trié
    post: affiche seulement les lignes ayant une occurence égale à 1 dans un fichier.txt selon uniq -u
    """
    lines = []
    dico = make_dic(f)
    for line,number in dico.items():
        if number < 2: #Si une ligne ne se répète pas
            lines.append(line)
    return lines

def skip_chars(f,n):
    """
    pre: f est un fichier.txt trié
         n est un nombre entier naturel
    post: retourne une liste(lines) contenant les lignes identiques avec une comparaison de type [N:] dans un fichier.txt selon uniq -s N
    """
    element = []
    lines = []
    with open(f,"r") as file:
        current = file.readline().strip()
        for line in file:
            if current[n:] == line.strip()[n:]: #Si la ligne enregistré est la meme que la ligne de [N:] (ex: aab == aac si N=1 ou N>3 mais != si N=2 ou N=3)
                if current not in element:  #Si pas encore dans la liste on affiche la ligne et on ajoute celle-ci dans une liste et elle devient la nouvelle ligne de comparaison
                    lines.append(line.strip())
                    element.append(current)
                    current = line.strip()
            else: #ligne différente de la précédente, on l'affiche et elle devient la nouvelle ligne de comparaison
                lines.append(line.strip())
                element.append(line.strip())
                current = line.strip()
        return lines

def check_chars(f,n):
    """
    pre: f est un fichier.txt trié
         n est un nombre entier naturel
    post: retourne une liste(lines) contenant les lignes identiques avec une comparaison de type [N:] dans un fichier.txt selon uniq -w N
    """
    element = []
    lines = []
    with open(f,"r") as file:
        current = file.readline().strip()
        for line in file:
            if current[:n] == line.strip()[:n]: #Si la ligne enregistré est la meme que la ligne de [:N] (ex: aab == aac si N<3 mais != si N>=3)
                if current not in element:
                    lines.append(line.strip())
                    element.append(current)
                    current = line.strip()
            else:
                lines.append(line.strip())
                element.append(line.strip())
                current = line.strip()
        return lines

analyzer = argparse.ArgumentParser(description = """Rassemble les lignes communes ensemble à leur 1ere occurence
Important: FICHIER doit etre trié avant d'utiliser uniq (Mettre : | sort -u (apres la commande uniq))""")
analyzer.add_argument("-c", "--count", action="store_true", help="Compte le nombre de lignes identiques")
analyzer.add_argument("-d", "--repeated", action="store_true", help="print seulement les lignes qui se répetent")
analyzer.add_argument("-u", "--unique", action="store_true", help="print seulement les lignes uniques")
analyzer.add_argument("-s", "--skip_chars", type=int, default=None, help="Compare qu'apres les N premiers caractères")
analyzer.add_argument("-w", "--check_chars",type=int, default=None, help="Compare seulement les N premiers caractères")
analyzer.add_argument('fichier', type=str, nargs='+', help="Le fichier qu'on analyse(trié de base)" )


args = analyzer.parse_args()

for i,fichier in enumerate(args.fichier):
    if len(args.fichier) > 1:
        if i > 0:
            print()
        print('==>', fichier, '<==')


    if args.skip_chars is not None: #uniq -s N
        if args.unique:
            if args.count:#uniq -s N -u -c
                dico = make_dic(fichier)
                lines = []
                u = unique(fichier)
                n =  args.skip_chars
                lines = skip_chars(fichier,n) 
                for line,number in dico.items():
                    if line in lines and line in u: #Si la ligne est dans la liste 
                        print("      " + str(1) + " " + line)
            else:#uniq -s N -u
                dico = make_dic(fichier)
                lines = []
                n =  args.skip_chars
                lines = skip_chars(fichier,n)
                u = unique(fichier)
                for line,number in dico.items():
                    if line in lines and line in u: #Si la ligne est dans la liste 
                        print(line)
                        
        elif args.repeated:
            if args.count: #uniq -s N -d -c
                dico = make_dic(fichier)
                lines = []
                d = repeated(fichier)
                n =  args.skip_chars
                lines = skip_chars(fichier,n) 
                for line,number in dico.items():
                    if line in lines and line in d: #Si la ligne est dans la liste 
                        print("      " + str(number) + " " + line)
                        
            else:#uniq -s N -d
                dico = make_dic(fichier)
                lines = []
                d = repeated(fichier)
                n =  args.skip_chars
                lines = skip_chars(fichier,n) 
                for line,number in dico.items():
                    if line in lines and line in d: #Si la ligne est dans la liste 
                        print(line)
                        
        elif args.count:#uniq -s N -c
            dico = make_dic(fichier)
            lines = []
            n =  args.skip_chars
            lines = skip_chars(fichier,n) 
            for line,number in dico.items():
                if line in lines: #Si la ligne est dans la liste 
                    print("      " + str(number) + " " + line)
        
        else:
            dico = make_dic(fichier)
            lines = []
            n =  args.skip_chars
            lines = skip_chars(fichier,n) 
            for line,number in dico.items():
                if line in lines: #Si la ligne est dans la liste 
                    print(line)
            
                
    elif args.check_chars is not None: #uniq -w N
        if args.unique:
            if args.count:#uniq -w N -u -c
                dico = make_dic(fichier)
                lines = []
                u = unique(fichier)
                n =  args.check_chars
                lines = check_chars(fichier,n) 
                for line,number in dico.items():
                    if line in lines and line in u: #Si la ligne est dans la liste 
                        print("      " + str(1) + " " + line)
                        
            else:#uniq -w N -u
                dico = make_dic(fichier)
                lines = []
                n =  args.check_chars
                lines = check_chars(fichier,n)
                u = unique(fichier)
                for line,number in dico.items():
                    if line in lines and line in u: #Si la ligne est dans la liste 
                        print(line)
                        
        elif args.repeated:
            if args.count: #uniq -w N -d -c
                dico = make_dic(fichier)
                lines = []
                d = repeated(fichier)
                n =  args.check_chars
                lines = check_chars(fichier,n) 
                for line,number in dico.items():
                    if line in lines and line in d: #Si la ligne est dans la liste 
                        print("      " + str(number) + " " + line)
                        
            else:#uniq -w N -d
                dico = make_dic(fichier)
                lines = []
                d = repeated(fichier)
                n =  args.check_chars
                lines = check_chars(fichier,n) 
                for line,number in dico.items():
                    if line in lines and line in d: #Si la ligne est dans la liste 
                        print(line)
                        
        elif args.count:#uniq -w N -c
            dico = make_dic(fichier)
            lines = []
            n =  args.check_chars
            lines = check_chars(fichier,n) 
            for line,number in dico.items():
                if line in lines: #Si la ligne est dans la liste 
                    print("      " + str(number) + " " + line)
        
        else:#uniq -w N
            dico = make_dic(fichier)
            lines = []
            n =  args.check_chars
            lines = check_chars(fichier,n) 
            for line,number in dico.items():
                if line in lines: #Si la ligne est dans la liste 
                    print(line)
    
    else: #Un seul argument
        if args.unique: #uniq -u
            dico = make_dic(fichier)
            u = unique(fichier)
            for line,number in dico.items():
                if line in u: #Si la ligne est dans la liste 
                    print(line)
                    
        elif args.repeated: #uniq -d
            dico = make_dic(fichier)
            d = repeated(fichier)
            for line,number in dico.items():
                if line in d: #Si la ligne est dans la liste 
                    print(line)
                    
        else: #uniq de base
            dico = make_dic(fichier)
            for line in dico.keys():
                print(line)
