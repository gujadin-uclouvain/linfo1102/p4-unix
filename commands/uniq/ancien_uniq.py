#!/usr/bin/env python3
import argparse

analyzer = argparse.ArgumentParser(description = """Rassemble les lignes communes ensemble à leur 1ere occurence
Important: FICHIER1 doit etre trié avant d'utiliser uniq (Mettre : | sort -u (apres la commande uniq))""")
analyzer.add_argument("-c", "--count", action="store_true", help="Compte le nombre de lignes identiques")
analyzer.add_argument("-d", "--repeated", action="store_true", help="print seulement les lignes qui se répetent")
analyzer.add_argument("-u", "--unique", action="store_true", help="print seulement les lignes uniques")
analyzer.add_argument("-s", "--skip_chars", type=int, default=None, help="Compare qu'apres les N premiers caractères")
analyzer.add_argument("-w", "--check_chars",type=int, default=None, help="Compare seulement les N premiers caractères")
analyzer.add_argument('fichier1', type=str, nargs='+', help="Le fichier qu'on analyse(trié de base)" )


args = analyzer.parse_args()

for i,fichier1 in enumerate(args.fichier1):
    if len(args.fichier1) > 1:
        if i > 0:
            print()
        print('==>', fichier1, '<==')

    with open(fichier1, "r") as file: #On ouvre le fichier qu'on souhaite analyser
        d={} 
        first = []
        line1 = file.readline().strip() #On sauvegarde la première ligne
        first.append(line1)
        d[line1] = 1 
        for line in file: #On passe sur chaque ligne et on ajoute dans un dico en comptant le nombre de répétition
            if (line.strip()) not in d:
                d[line.strip()] = 1
            else:
                d[line.strip()] += 1

        if args.count: #uniq -c
            for l,nombre in d.items(): #On passe dans le dico et on retourne combien de fois se répète chaque ligne
                print("      "+str(nombre)+" "+l)

        elif args.repeated: #uniq -d
            for l,nombre in d.items():
                if nombre > 1: #Si une ligne se répète plus d'une fois
                    print(l)
        elif args.unique: #uniq -u
            for l,nombre in d.items():
                if nombre < 2: #Si une ligne ne se répète pas
                    print(l)
        elif args.skip_chars is not None: #uniq -s N
            n = args.skip_chars #le nombre apres le -s 
            element = []
            current = first[0]
            for l in d.keys():
                if current[n:] == l[n:]: #Si la ligne enregistré est la meme que la ligne de [N:] (ex: aab == aac si N=1 ou N>3 mais != si N=2 ou N=3)
                    if current not in element:  #Si pas encore dans la liste on affiche la ligne et on ajoute celle-ci dans une liste et elle devient la nouvelle ligne de comparaison
                        print(l)
                        element.append(current)
                        current = l
                else: #ligne différente de la précédente, on l'affiche et elle devient la nouvelle ligne de comparaison
                    print(l)
                    current = l

        elif args.check_chars is not None: #uniq -w N
            n = args.check_chars
            element = []
            current = first[0]
            for l in d.keys():
                if current[:n] == l[:n]: #Si la ligne enregistré est la meme que la ligne de [:N] (ex: aab == aac si N<3 mais != si N>=3)
                    if current not in element:
                        print(l)
                        element.append(current)
                        current = l
                else:
                    print(l)
                    element.append(l)
                    current = l



        else: #uniq de base
            for l in d.keys(): 
                print(l)
