#!/usr/bin/env python3
import argparse

def make_dic(f):
    """
    Pre: f est un fichier.txt trié
    Post: retourne un dictionnaire contenant pour chaque ligne différente son occurence ( {ligne: occurence} )
    """
    with open(f, "r") as file:
        dico={} #initialise dictionnaire
        for line in file: #On passe sur chaque ligne et on ajoute dans le dico(si pas déjà présent) en comptant le nombre de répétition
            if line.strip() not in dico:
                dico[line.strip()] = 1
            else:
                dico[line.strip()] += 1
        return dico

def repeated(f):
    """
    pre: f est un fichier.txt trié
    post: retourne une liste (lines) contenant seulement les lignes ayant une occurence strictement supérieure à 1 dans un fichier.txt selon uniq -d
    """
    lines = []
    dico = make_dic(f)
    for line,number in dico.items():
        if number > 1: #Si une ligne se répète plus d'une fois
            lines.append(line)
    return lines

def unique(f):
    """
    pre: f est un fichier.txt trié
    post: affiche seulement les lignes ayant une occurence égale à 1 dans un fichier.txt selon uniq -u
    """
    lines = []
    dico = make_dic(f)
    for line,number in dico.items():
        if number < 2: #Si une ligne ne se répète pas
            lines.append(line)
    return lines

def skip_chars(f,n):
    """
    pre: f est un fichier.txt trié
         n est un nombre entier naturel
    post: retourne une liste(lines) contenant les lignes identiques avec une comparaison de type [N:] dans un fichier.txt selon uniq -s N
    """
    element = []
    lines = []
    current = ""
    with open(f,"r") as file:
        while current == "": #évite de prendre les éventuelles lignes vides du début du fichier trié
            current = file.readline().strip()
        for line in file:
            if current[n:] == line.strip()[n:]: #Si la ligne enregistré est la meme que la ligne de [N:] (ex: aab == aac si N=1 ou N>3 mais != si N=2 ou N=3)
                if current not in element:  #Si pas encore dans la liste on ajoute la ligne à la liste(lines) et on ajoute current dans une liste(element) pour les prochaines comparaisons
                    lines.append(line.strip())
                    element.append(current)
                    current = line.strip()
            else: #ligne différente de la précédente, on l'ajoute à la liste et elle devient la nouvelle ligne de comparaison
                lines.append(line.strip())
                element.append(line.strip())
                current = line.strip()
        return lines

def check_chars(f,n):
    """
    pre: f est un fichier.txt trié
         n est un nombre entier naturel
    post: retourne une liste(lines) contenant les lignes identiques avec une comparaison de type [:N] dans un fichier.txt selon uniq -w N
    """
    element = []
    lines = []
    current = ""
    with open(f,"r") as file:
        while current == "": #évite de prendre les éventuelles lignes vides du début du fichier trié
            current = file.readline().strip()
        for line in file:
            if current[:n] == line.strip()[:n]: #Si la ligne enregistré est la meme que la ligne de [:N] (ex: aab == aac si N<3 mais != si N>=3)
                if current not in element:
                    lines.append(line.strip())
                    element.append(current)
                    current = line.strip()
            else:
                lines.append(line.strip())
                element.append(line.strip())
                current = line.strip()
        return lines

def affichage_skip(m,count,combinaison):
    """
    pre: m est un appel à une fonction (soit unique(fichier), soit repeated(fichier)) qui renvoit une liste
         count est un booléen (Soit True soit False)
         combinaison est un boléen (soit True soit False)
    post: affiche les lignes comme uniq -s N -u -c (si m =unique(fichier), count est True et combinaison est True)
          affiche les lignes comme uniq -s N -u (si m =unique(fichier), count est False et combinaison est True)
          affiche les lignes comme uniq -s N -d (si m =repeated(fichier), count est False et combinaison est True)
          affiche les lignes comme uniq -s N (si m = None, count est False et combinaison est False)
    """
    dico = make_dic(fichier)
    n =  args.skip_chars
    lines = skip_chars(fichier,n)
    for line,number in dico.items():
        if combinaison:
            if line in lines and line in m: #Si la ligne est dans les listes (lines et m)
                if count:
                    print("      " + str(1) + " " + line)
                else:
                    print(line)
        else:
            if line in lines: #Si la ligne est dans la liste lines
                print(line)

def affichage_check_unique(count):
    """
    pre: count est un booléen (True ou False)
    post: affiche les lignes comme uniq -w N -u -c (si count est True)
          affiche les lignes comme uniq -w N -u (si count est False)
    """
    dico = make_dic(fichier)
    n = args.check_chars
    l = [] #liste pour stocker les lignes à afficher
    occ = 0 #variable du nombre d'occurence
    for line,number in dico.items():
        if number == 1: #Si 1 seul occurence de la ligne
            for e in dico.keys(): #on boucle avec les autres lignes
                if line[:n] == e[:n]:
                    occ += 1
            if occ == 1: #Si apres comparaison avec les autres lignes, il n'y a qu'une seul occurence : on ajoute la ligne à la liste
                l.append(line)
            occ = 0
    for e in l: #on boucle les différentes lignes à afficher
        if count:
            print("      " + str(1) + " " + e)
        else:
            print(e)

def affichage_check_repeated(count):
    """
    pre: count est un booléen (True ou False)
    post: affiche les lignes comme uniq -w N -d -c (si count est True)
          affiche les lignes comme uniq -w N -d (si count est False)
    """
    dico = make_dic(fichier)
    n =  args.check_chars
    lines = check_chars(fichier,n)
    occ = 0
    for line in lines: #On boucles les lignes de la listes lines qui contient les lignes unique apres comparaison [:N]
        for e,num in dico.items():#on boucle les lignes du dico pour comparer les lignes de lines avec toutes les lignes du fichier(sans répétition)
            if line[:n] == e[:n] or e[:n] == "":
                occ += num
        if occ > 1:#Si la ligne se répete plus d'une fois avec la comparaison [:N], on l'affiche
            if count:
                print("      " + str(occ) + " " + line)
            else:
                print(line)
        occ = 0


analyzer = argparse.ArgumentParser(description = """Rassemble les lignes communes ensemble à leur 1ere occurence
Important: FICHIER doit etre trié avant d'utiliser uniq (Mettre : | sort -u (apres la commande uniq))""")
analyzer.add_argument("-c", "--count", action="store_true", help="Compte le nombre de lignes identiques")
analyzer.add_argument("-d", "--repeated", action="store_true", help="print seulement les lignes qui se répetent")
analyzer.add_argument("-u", "--unique", action="store_true", help="print seulement les lignes uniques")
analyzer.add_argument("-s", "--skip_chars", type=int, default=None, help="Compare qu'apres les N premiers caractères")
analyzer.add_argument("-w", "--check_chars",type=int, default=None, help="Compare seulement les N premiers caractères")
analyzer.add_argument('fichier', type=str, nargs='+', help="Le fichier qu'on analyse(trié de base)" )

args = analyzer.parse_args()

for i,fichier in enumerate(args.fichier):
    if len(args.fichier) > 1:
        if i > 0:
            print()
        print('==>', fichier, '<==')


    if args.skip_chars is not None: #uniq -s N
        if args.unique:
            if args.count:
                if args.repeated: #uniq -s N -u -c -d
                    pass #Ne fait rien
                else:#uniq -s N -u -c
                    affichage_skip(unique(fichier),True,True)

            else:#uniq -s N -u
                affichage_skip(unique(fichier),False,True)

        elif args.repeated:
            if args.count: #uniq -s N -d -c
                dico = make_dic(fichier)
                d = repeated(fichier)
                n =  args.skip_chars
                lines = skip_chars(fichier,n)
                l = [] #liste pour stocker les lignes à afficher
                occ = 0 #variable qui stock l'occurence de la ligne
                for line,number in dico.items():
                    if line in lines and line in d: #Si la ligne est dans la liste lines et dans la liste d
                        l.append(line)

                if len(l) > 0: #S'il la liste contient quelque chose
                    for e in l:
                        for line,num in dico.items(): #On compare chaque élement de la liste l avec les lignes du dico selon la comparaison [N:]
                            if e[n:] == line[n:]: #Si les lignes sont identiques, on augment la variable occ par le nombre d'occurence de cette ligne
                                occ += num
                            else: #Si les lignes sont différentes on regarde si c'est le 1er élement de la liste l (Oui: on break la boucle sinon on continue)
                                if e == l[0]:
                                    break
                        print("      " + str(occ) + " " + e)
                        occ = 0


            else:#uniq -s N -d
                affichage_skip(repeated(fichier),False,True)

        elif args.count:#uniq -s N -c
            dico = make_dic(fichier)
            n =  args.skip_chars
            lines = skip_chars(fichier,n)
            add_num = 0 #variable contenant la somme des occurence des lignes parcourus (revient à 0 après chaque affichage d'une ligne)
            first = "" #variable qui contientiendra la 1ère ligne de la liste lines
            for line,number in dico.items():
                if line in lines and line != lines[0]:
                    if first != "": #Si la variable first contient quelque chose
                        print("      " + str(add_num) + " " + first)
                        first = ""
                    print("      " + str(number) + " " + line)
                    add_num = 0
                elif line in lines and line == lines[0]:
                    first = line
                add_num += number
            if first != "": #si la variable first contient quelque chose à la fin de la boucle
                print("      " + str(add_num) + " " + first)

        else: #uniq -s N
            affichage_skip(None,False,False)

    elif args.check_chars is not None: #uniq -w N
        if args.unique:
            if args.count:#uniq -w N -u -c
                if args.repeated: #uniq -w N -u -c -d
                    pass #Ne fait rien
                else:
                    affichage_check_unique(True)

            else:#uniq -w N -u
                affichage_check_unique(False)

        elif args.repeated:
            if args.count: #uniq -w N -d -c
                affichage_check_repeated(True)

            else:#uniq -w N -d
                affichage_check_repeated(False)

        elif args.count:#uniq -w N -c
            dico = make_dic(fichier)
            n =  args.check_chars
            lines = check_chars(fichier,n)
            add_num = 0 #variable contenant la somme des occurence des lignes parcourus (revient à 0 après chaque affichage d'une ligne)
            first = "" #variable qui contientiendra la 1ère ligne de la liste lines
            temp = "" #variable qui contientiendra la ligne précédente de la boucle afin de la comparer avec la suivante
            for line,number in dico.items():
                if line in lines and line != lines[0]: #Si la ligne est dans la liste lines et que c'est pas la 1ère ligne de la liste
                    if first != "": #first est assigné à une ligne
                        print("      " + str(add_num) + " " + first)
                        first = ""
                        add_num = 0
                    if first == "": #first a déjà été assigné avant
                        if line != temp and temp != "": #la ligne est différente que la précédente
                            print("      " + str(add_num) + " " + temp)
                            add_num = 0
                        temp = line
                elif line in lines and line == lines[0]: #Si la ligne est dans la liste lines et que c'est la 1ère ligne de la liste
                    first = line
                add_num += number
            if first != "":
                print("      " + str(add_num) + " " + first)
            if temp != "":
                print("      " + str(add_num) + " " + temp)

        else:#uniq -w N
            dico = make_dic(fichier)
            lines = []
            n =  args.check_chars
            lines = check_chars(fichier,n)
            for line,number in dico.items():
                if line in lines: #Si la ligne est dans la liste
                    print(line)

    elif args.unique:
        if args.count: #uniq -u -c
            dico = make_dic(fichier)
            u = unique(fichier)
            for line,number in dico.items():
                if line in u: #Si la ligne est dans la liste u
                    print("      " + str(number) + " " + line)

        else: #uniq -u
            dico = make_dic(fichier)
            u = unique(fichier)
            for line,number in dico.items():
                if line in u: #Si la ligne est dans la liste u
                    print(line)

    elif args.repeated:
        if args.count: #uniq -d -c
            dico = make_dic(fichier)
            d = repeated(fichier)
            for line,number in dico.items():
                if line in d: #Si la ligne est dans la liste d
                    print("      " + str(number) + " " + line)

        else:#uniq -d
            dico = make_dic(fichier)
            d = repeated(fichier)
            for line,number in dico.items():
                if line in d: #Si la ligne est dans la liste d
                    print(line)

    else: #Un seul argument
        if args.count: #uniq -c
            dico = make_dic(fichier)
            for line,number in dico.items():
                print("      " + str(number) + " " + line)



        else: #uniq de base
            dico = make_dic(fichier)
            for line in dico.keys():
                print(line)
