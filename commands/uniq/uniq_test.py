import os
import subprocess
import unittest

file1_txt = \
"""
aa
aa
b
ba
de
dge
dge
feh aa
"""



def uniq(*args):
    try:
        return subprocess.check_output(['python3', 'v2_uniq.py', *args], universal_newlines=True)
    except subprocess.CalledProcessError as e:
        return e.outpout

class TestUniq(unittest.TestCase):
    #"""Class de test"""

    def setUp(self):
        #"""Initialisation des tests"""
        with open("file1.txt", "w") as f:
            f.write(file1_txt.strip())

    def tearDown(self):
        os.unlink("file1.txt")

    def test_uniq(self):
        #"""Test la commande de base"""
        self.assertEqual(uniq("file1.txt").strip(), "aa\nb\nba\nde\ndge\nfeh aa".strip())


    def test_c(self):
        #"""test uniq -c"""
        self.assertEqual(uniq("file1.txt","-c").rstrip(), "      2 aa\n      1 b\n      1 ba\n      1 de\n      2 dge\n      1 feh aa")


    def test_d(self):
        #"""test uniq -d"""
        self.assertEqual(uniq("file1.txt","-d").strip(),"aa\ndge")
        self.assertEqual(uniq("file1.txt","-d", "-c").strip(),"      2 aa\n      2 dge".strip())

    def test_s(self):
        #"""test uniq -s N"""
        self.assertEqual(uniq("file1.txt","-s", "0").strip(),"aa\nb\nba\nde\ndge\nfeh aa".strip())
        self.assertEqual(uniq("file1.txt","-s", "1").strip(),"aa\nb\nba\nde\ndge\nfeh aa".strip())
        self.assertEqual(uniq("file1.txt","-s", "2").strip(),"aa\ndge\nfeh aa".strip())
        self.assertEqual(uniq("file1.txt","-s", "3").strip(),"aa\nfeh aa".strip())
        self.assertEqual(uniq("file1.txt","-s", "4").strip(),"aa\nfeh aa".strip())
        self.assertEqual(uniq("file1.txt","-s", "6").strip(),"aa".strip())

    def test_u(self):
        """test uniq -u"""
        self.assertEqual(uniq("file1.txt","-u").strip(),"b\nba\nde\nfeh aa".strip())

    def test_w(self):
        #"""test uniq -w N"""
        self.assertEqual(uniq("file1.txt","-w", "4").strip(),"aa\nb\nba\nde\ndge\nfeh aa".strip())
        self.assertEqual(uniq("file1.txt","-w", "3").strip(),"aa\nb\nba\nde\ndge\nfeh aa".strip())
        self.assertEqual(uniq("file1.txt","-w", "2").strip(),"aa\nb\nba\nde\ndge\nfeh aa".strip())
        self.assertEqual(uniq("file1.txt","-w", "1").strip(),"aa\nb\nde\nfeh aa".strip())
        self.assertEqual(uniq("file1.txt","-w", "0").strip(),"aa".strip())

    def test_u_c(self):
        """test uniq -u -c"""
        self.assertEqual(uniq("file1.txt","-c","-u").strip(),"      1 b\n      1 ba\n      1 de\n      1 feh aa".strip())
        self.assertEqual(uniq("file1.txt","-u","-c").strip(),"      1 b\n      1 ba\n      1 de\n      1 feh aa".strip())

    def test_s_autres(self):
        self.assertEqual(uniq("file1.txt","-s", "0", "-c").strip(),"      2 aa\n      1 b\n      1 ba\n      1 de\n      2 dge\n      1 feh aa".strip())
        self.assertEqual(uniq("file1.txt","-c", "-s", "0").strip(),"      2 aa\n      1 b\n      1 ba\n      1 de\n      2 dge\n      1 feh aa".strip())
        self.assertEqual(uniq("file1.txt","-s", "1", "-c").strip(),"      2 aa\n      1 b\n      1 ba\n      1 de\n      2 dge\n      1 feh aa".strip())
        self.assertEqual(uniq("file1.txt","-c", "-s", "1").strip(),"      2 aa\n      1 b\n      1 ba\n      1 de\n      2 dge\n      1 feh aa".strip())
        self.assertEqual(uniq("file1.txt","-c","-s", "2").strip(),"      5 aa\n      2 dge\n      1 feh aa".strip())
        self.assertEqual(uniq("file1.txt","-s","2", "-c").strip(),"      5 aa\n      2 dge\n      1 feh aa".strip())
        self.assertEqual(uniq("file1.txt","-c","-s", "3").strip(),"      7 aa\n      1 feh aa".strip())
        self.assertEqual(uniq("file1.txt","-s","3", "-c").strip(),"      7 aa\n      1 feh aa".strip())
        self.assertEqual(uniq("file1.txt","-s", "6", "-c").strip(),"      8 aa".strip())
        self.assertEqual(uniq("file1.txt","-c", "-s", "6").strip(),"      8 aa".strip())

        self.assertEqual(uniq("file1.txt","-s", "0", "-u").strip(),"b\nba\nde\nfeh aa".strip())
        self.assertEqual(uniq("file1.txt","-s", "1", "-u").strip(),"b\nba\nde\nfeh aa".strip())
        self.assertEqual(uniq("file1.txt","-s", "2", "-u").strip(),"feh aa".strip())
        self.assertEqual(uniq("file1.txt","-s", "8", "-u").strip(),"")

        self.assertEqual(uniq("file1.txt","-s", "0", "-d").strip(),"aa\ndge".strip())
        self.assertEqual(uniq("file1.txt","-s", "1", "-d").strip(),"aa\ndge".strip())
        self.assertEqual(uniq("file1.txt","-s", "2", "-d").strip(),"aa\ndge".strip())
        self.assertEqual(uniq("file1.txt","-s", "3", "-d").strip(),"aa".strip())

        self.assertEqual(uniq("file1.txt","-s", "0", "-u", "-c").strip(),"      1 b\n      1 ba\n      1 de\n      1 feh aa".strip())
        self.assertEqual(uniq("file1.txt","-s", "1", "-u", "-c").strip(),"      1 b\n      1 ba\n      1 de\n      1 feh aa".strip())
        self.assertEqual(uniq("file1.txt","-s", "2", "-u", "-c").strip(),"      1 feh aa".strip())
        self.assertEqual(uniq("file1.txt","-s", "8", "-u", "-c").strip(),"")

        self.assertEqual(uniq("file1.txt","-s", "0", "-d", "-c").strip(),"      2 aa\n      2 dge".strip())
        self.assertEqual(uniq("file1.txt","-d", "-c", "-s", "0").strip(),"      2 aa\n      2 dge".strip())
        self.assertEqual(uniq("file1.txt","-s", "2", "-d", "-c").strip(),"      5 aa\n      2 dge".strip())
        self.assertEqual(uniq("file1.txt","-d", "-c", "-s", "2").strip(),"      5 aa\n      2 dge".strip())
        self.assertEqual(uniq("file1.txt","-s", "3", "-d", "-c").strip(),"      7 aa".strip())
        self.assertEqual(uniq("file1.txt","-d", "-c", "-s", "3").strip(),"      7 aa".strip())
        self.assertEqual(uniq("file1.txt","-s", "8", "-d", "-c").strip(),"      8 aa".strip())
        self.assertEqual(uniq("file1.txt","-d", "-c", "-s", "8").strip(),"      8 aa".strip())

        self.assertEqual(uniq("file1.txt","-d", "-c", "-s", "0", "-u").strip(),"")

    def test_w_autres(self):
        self.assertEqual(uniq("file1.txt","-w", "0", "-c").strip(),"      8 aa".strip())
        self.assertEqual(uniq("file1.txt","-c", "-w", "0").strip(),"      8 aa".strip())
        self.assertEqual(uniq("file1.txt","-w", "1", "-c").strip(),"      2 aa\n      2 b\n      3 de\n      1 feh aa".strip())
        self.assertEqual(uniq("file1.txt","-c", "-w", "1").strip(),"      2 aa\n      2 b\n      3 de\n      1 feh aa".strip())
        self.assertEqual(uniq("file1.txt","-w", "2", "-c").strip(),"      2 aa\n      1 b\n      1 ba\n      1 de\n      2 dge\n      1 feh aa".strip())
        self.assertEqual(uniq("file1.txt","-c", "-w", "2").strip(),"      2 aa\n      1 b\n      1 ba\n      1 de\n      2 dge\n      1 feh aa".strip())
        self.assertEqual(uniq("file1.txt","-w", "8", "-c").strip(),"      2 aa\n      1 b\n      1 ba\n      1 de\n      2 dge\n      1 feh aa".strip())
        self.assertEqual(uniq("file1.txt","-c", "-w", "8").strip(),"      2 aa\n      1 b\n      1 ba\n      1 de\n      2 dge\n      1 feh aa".strip())

        self.assertEqual(uniq("file1.txt","-w", "0", "-u").strip(),"")
        self.assertEqual(uniq("file1.txt","-w", "1", "-u").strip(),"feh aa".strip())
        self.assertEqual(uniq("file1.txt","-w", "2", "-u").strip(),"b\nba\nde\nfeh aa".strip())
        self.assertEqual(uniq("file1.txt","-w", "8", "-u").strip(),"b\nba\nde\nfeh aa".strip())
        self.assertEqual(uniq("file1.txt","-u", "-w", "8").strip(),"b\nba\nde\nfeh aa".strip())

        self.assertEqual(uniq("file1.txt","-w", "0", "-d").strip(),"aa".strip())
        self.assertEqual(uniq("file1.txt","-w", "1", "-d").strip(),"aa\nb\nde".strip())
        self.assertEqual(uniq("file1.txt","-w", "2", "-d").strip(),"aa\ndge".strip())
        self.assertEqual(uniq("file1.txt","-w", "8", "-d").strip(),"aa\ndge".strip())

        self.assertEqual(uniq("file1.txt","-w", "0", "-u", "-c").strip(),"".strip())
        self.assertEqual(uniq("file1.txt","-w", "1", "-u", "-c").strip(),"      1 feh aa".strip())
        self.assertEqual(uniq("file1.txt","-w", "2", "-u", "-c").strip(),"      1 b\n      1 ba\n      1 de\n      1 feh aa".strip())

        self.assertEqual(uniq("file1.txt","-w", "0", "-d", "-c").strip(),"      8 aa".strip())
        self.assertEqual(uniq("file1.txt","-d", "-c", "-w", "0").strip(),"      8 aa".strip())
        self.assertEqual(uniq("file1.txt","-w", "1", "-d", "-c").strip(),"      2 aa\n      2 b\n      3 de".strip())
        self.assertEqual(uniq("file1.txt","-w", "2", "-d", "-c").strip(),"      2 aa\n      2 dge".strip())
        self.assertEqual(uniq("file1.txt","-w", "8", "-d", "-c").strip(),"      2 aa\n      2 dge".strip())

        self.assertEqual(uniq("file1.txt","-d", "-c", "-w", "0", "-u").strip(),"")

if __name__ == '__main__':
    unittest.main()
