#!/usr/bin/env python3
import argparse

def make_dic(f):
    """
    Pre: f est un fichier.txt trié qu'on analyse
    Post: retourne un dictionnaire contenant la ligne et son occurence ( {ligne: occurence} )
    """
    with open(f, "r") as file:  #On ouvre le fichier qu'on souhaite analyser
        dico={} #initialise dictionnaire
        for line in file: #On passe sur chaque ligne et on ajoute dans le dico en comptant le nombre de répétition
            if line.strip() not in dico:
                dico[line.strip()] = 1
            else:
                dico[line.strip()] += 1
        return dico


def repeated(f):
    """
    pre: f est un fichier.txt trié
    post: retourne une liste (lines) contenant seulement les lignes ayant une occurence strictement supérieure à 1 dans un fichier.txt selon uniq -d
    """
    lines = []
    dico = make_dic(f)
    for line,number in dico.items():
        if number > 1: #Si une ligne se répète plus d'une fois
            lines.append(line)
    return lines

def unique(f):
    """
    pre: f est un fichier.txt trié
    post: affiche seulement les lignes ayant une occurence égale à 1 dans un fichier.txt selon uniq -u
    """
    lines = []
    dico = make_dic(f)
    for line,number in dico.items():
        if number < 2: #Si une ligne ne se répète pas
            lines.append(line)
    return lines

def skip_chars(f,n):
    """
    pre: f est un fichier.txt trié
         n est un nombre entier naturel
    post: retourne une liste(lines) contenant les lignes identiques avec une comparaison de type [N:] dans un fichier.txt selon uniq -s N
    """
    element = []
    lines = []
    current = ""
    with open(f,"r") as file:
        while current == "": #évite de prendre les éventuels lignes vides du début du fichier trié
            current = file.readline().strip()
        for line in file:
            if current[n:] == line.strip()[n:]: #Si la ligne enregistré est la meme que la ligne de [N:] (ex: aab == aac si N=1 ou N>3 mais != si N=2 ou N=3)
                if current not in element:  #Si pas encore dans la liste on affiche la ligne et on ajoute celle-ci dans une liste et elle devient la nouvelle ligne de comparaison
                    lines.append(line.strip())
                    element.append(current)
                    current = line.strip()
            else: #ligne différente de la précédente, on l'affiche et elle devient la nouvelle ligne de comparaison
                lines.append(line.strip())
                element.append(line.strip())
                current = line.strip()
        return lines

def check_chars(f,n):
    """
    pre: f est un fichier.txt trié
         n est un nombre entier naturel
         s est un nombre entier naturel
    post: retourne une liste(lines) contenant les lignes identiques avec une comparaison de type [:N] dans un fichier.txt selon uniq -w N
    """
    element = []
    lines = []
    current = ""
    with open(f,"r") as file:
        while current == "": #évite de prendre les éventuels lignes vides du début du fichier trié
            current = file.readline().strip()
        for line in file:
            if current[:n] == line.strip()[:n]: #Si la ligne enregistré est la meme que la ligne de [:N] (ex: aab == aac si N<3 mais != si N>=3)
                if current not in element:
                    lines.append(line.strip())
                    element.append(current)
                    current = line.strip()
            else:
                lines.append(line.strip())
                element.append(line.strip())
                current = line.strip()
        return lines

analyzer = argparse.ArgumentParser(description = """Rassemble les lignes communes ensemble à leur 1ere occurence
Important: FICHIER doit etre trié avant d'utiliser uniq (Mettre : | sort -u (apres la commande uniq))""")
analyzer.add_argument("-c", "--count", action="store_true", help="Compte le nombre de lignes identiques")
analyzer.add_argument("-d", "--repeated", action="store_true", help="print seulement les lignes qui se répetent")
analyzer.add_argument("-u", "--unique", action="store_true", help="print seulement les lignes uniques")
analyzer.add_argument("-s", "--skip_chars", type=int, default=None, help="Compare qu'apres les N premiers caractères")
analyzer.add_argument("-w", "--check_chars",type=int, default=None, help="Compare seulement les N premiers caractères")
analyzer.add_argument('fichier', type=str, nargs='+', help="Le fichier qu'on analyse(trié de base)" )


args = analyzer.parse_args()

for i,fichier in enumerate(args.fichier):
    if len(args.fichier) > 1:
        if i > 0:
            print()
        print('==>', fichier, '<==')


    if args.skip_chars is not None: #uniq -s N
        if args.unique:
            if args.count:#uniq -s N -u -c
                if args.repeated: #uniq -s N -u -c -d
                    pass #Ne fait rien
                else:
                    dico = make_dic(fichier)
                    lines = []
                    u = unique(fichier)
                    n =  args.skip_chars
                    lines = skip_chars(fichier,n)
                    for line,number in dico.items():
                        if line in lines and line in u: #Si la ligne est dans la liste
                            print("      " + str(1) + " " + line)
            else:#uniq -s N -u
                dico = make_dic(fichier)
                n =  args.skip_chars
                lines = skip_chars(fichier,n)
                u = unique(fichier)
                for line,number in dico.items():
                    if line in lines and line in u: #Si la ligne est dans la liste
                        print(line)

        elif args.repeated:
            if args.count: #uniq -s N -d -c
                dico = make_dic(fichier)
                d = repeated(fichier)
                n =  args.skip_chars
                lines = skip_chars(fichier,n)
                l = []
                occ = 0
                for line,number in dico.items():
                    if line in lines and line in d: #Si la ligne est dans la liste
                        l.append(line)
                if len(l) > 0:
                    for e in l:
                        for line,num in dico.items():
                            if e[n:] == line[n:]:
                                occ += num
                            else:
                                if e == l[0]:
                                    break
                        print("      " + str(occ) + " " + e)
                        occ = 0


            else:#uniq -s N -d
                dico = make_dic(fichier)
                d = repeated(fichier)
                n =  args.skip_chars
                lines = skip_chars(fichier,n)
                for line,number in dico.items():
                    if line in lines and line in d: #Si la ligne est dans la liste
                        print(line)

        elif args.count:#uniq -s N -c
            dico = make_dic(fichier)
            lines = []
            n =  args.skip_chars
            lines = skip_chars(fichier,n)
            add_num = 0
            first = ""
            for line,number in dico.items():
                if line in lines and line != lines[0]:
                    if first != "":
                        print("      " + str(add_num) + " " + first)
                        first = ""
                    print("      " + str(number) + " " + line)
                    add_num = 0
                elif line in lines and line == lines[0]:
                    first = line
                add_num += number
            if first != "":
                print("      " + str(add_num) + " " + first)



        else: #uniq -s N
            dico = make_dic(fichier)
            lines = []
            n =  args.skip_chars
            lines = skip_chars(fichier,n)
            for line,number in dico.items():
                if line in lines: #Si la ligne est dans la liste
                    print(line)


    elif args.check_chars is not None: #uniq -w N
        if args.unique:
            if args.count:#uniq -w N -u -c
                if args.repeated: #uniq -w N -u -c -d
                    pass #Ne fait rien
                else:
                    dico = make_dic(fichier)
                    lines = []
                    u = unique(fichier)
                    n =  args.check_chars
                    lines = check_chars(fichier,n)
                    l = []
                    occ = 0
                    for line,number in dico.items():
                        if number == 1:
                            for e in dico.keys():
                                if line[:n] == e[:n]:
                                    occ += 1
                            if occ == 1:
                                l.append(line)
                            occ = 0
                    for e in l:
                        print("      " + str(1) + " " + e)

            else:#uniq -w N -u
                dico = make_dic(fichier)
                lines = []
                n =  args.check_chars
                lines = check_chars(fichier,n)
                u = unique(fichier)
                l = []
                occ = 0
                for line,number in dico.items():
                    if number == 1:
                        for e in dico.keys():
                            if line[:n] == e[:n]:
                                occ += 1
                        if occ == 1:
                            l.append(line)
                        occ = 0
                for e in l:
                    print(e)

        elif args.repeated:
            if args.count: #uniq -w N -d -c
                dico = make_dic(fichier)
                n =  args.check_chars
                lines = check_chars(fichier,n)
                l = []
                occ = 0
                for line in lines:
                    for e,num in dico.items():
                        if line[:n] == e[:n] or e[:n] == "":
                            occ += num
                    if occ > 1:
                        print("      " + str(occ) + " " + line)
                    occ = 0

            else:#uniq -w N -d
                dico = make_dic(fichier)
                d = repeated(fichier)
                n =  args.check_chars
                lines = check_chars(fichier,n)
                l = []
                occ = 0
                for line in lines:
                    for e,num in dico.items():
                        if line[:n] == e[:n] or e[:n] == "":
                            occ += num
                    if occ > 1:
                        print(line)
                    occ = 0

        elif args.count:#uniq -w N -c
            dico = make_dic(fichier)
            lines = []
            n =  args.check_chars
            lines = check_chars(fichier,n)
            add_num = 0
            first = ""
            temp = ""
            for line,number in dico.items():
                if line in lines and line != lines[0]:
                    if first != "":
                        print("      " + str(add_num) + " " + first)
                        first = ""
                        add_num = 0
                    if first == "":
                        if line != temp and temp != "":
                            print("      " + str(add_num) + " " + temp)
                            add_num = 0
                        temp = line
                elif line in lines and line == lines[0]:
                    first = line
                add_num += number
            if first != "":
                print("      " + str(add_num) + " " + first)
            if temp != "":
                print("      " + str(add_num) + " " + temp)



        else:#uniq -w N
            dico = make_dic(fichier)
            lines = []
            n =  args.check_chars
            lines = check_chars(fichier,n)
            for line,number in dico.items():
                if line in lines: #Si la ligne est dans la liste
                    print(line)

    elif args.unique:
        if args.count: #uniq -u -c
            dico = make_dic(fichier)
            u = unique(fichier)
            for line,number in dico.items():
                if line in u: #Si la ligne est dans la liste
                    print("      " + str(number) + " " + line)
        else: #uniq -u
            dico = make_dic(fichier)
            u = unique(fichier)
            for line,number in dico.items():
                if line in u: #Si la ligne est dans la liste
                    print(line)

    elif args.repeated:
        if args.count: #uniq -d -c
            dico = make_dic(fichier)
            d = repeated(fichier)
            for line,number in dico.items():
                if line in d: #Si la ligne est dans la liste
                    print("      " + str(number) + " " + line)

        else:#uniq -d
            dico = make_dic(fichier)
            d = repeated(fichier)
            for line,number in dico.items():
                if line in d: #Si la ligne est dans la liste
                    print(line)

    else: #Un seul argument
        if args.count: #uniq -c
            dico = make_dic(fichier)
            for line,number in dico.items():
                print("      " + str(number) + " " + line)



        else: #uniq de base
            dico = make_dic(fichier)
            for line in dico.keys():
                print(line)
